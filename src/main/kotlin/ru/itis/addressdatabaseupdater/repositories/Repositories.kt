package ru.itis.addressdatabaseupdater.repositories

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.dto.FilePath
import ru.itis.addressdatabaseupdater.models.*
import ru.itis.addressdatabaseupdater.models.Code2CharAlias
import java.util.Optional

interface LanguagesRepository: JpaRepository<Iso639FirstLevel, Long> {
    fun findIso639FirstLevelByCode2char(code2Char: Code2CharAlias): Iso639FirstLevel?
}

interface CountriesRepository: JpaRepository<Iso3166FirstLevel, Long> {

    fun findIso3166FirstLevelByCode2char(code2Char: Code2CharAlias): Iso3166FirstLevel?
}

interface SubdivisionsRepository: JpaRepository<Iso3166SecondLevel<*,*>, Long> {
    fun findIso3166SecondLevelByCode3char(code3Char: Code3CharAlias): Iso3166SecondLevel<*,*>?
    fun findIso3166SecondLevelsByDescendantsProtectedIsNull(): List<Iso3166SecondLevel<*,*>>
    fun findIso3166SecondLevelsByParentProtectedIsNull(): List<Iso3166SecondLevel<*,*>>
    fun findIso3166SecondLevelsByDescendantsProtectedIsNotNull(): List<Iso3166SecondLevel<*,*>>
    fun findIso3166SecondLevelsByParentProtectedIsNotNull(): List<Iso3166SecondLevel<*,*>>
//    Lonely
    fun findIso3166SecondLevelsByDescendantsProtectedIsNullAndParentProtectedIsNull(): List<Iso3166SecondLevel<*,*>>
    //    OnlyChild
    fun findIso3166SecondLevelsByDescendantsProtectedIsNullAndParentProtectedIsNotNull(): List<Iso3166SecondLevel<*,*>>
//    OnlyParent
    fun findIso3166SecondLevelsByDescendantsProtectedIsNotNullAndParentProtectedIsNull(): List<Iso3166SecondLevel<*,*>>
//    Surrounded
    fun findIso3166SecondLevelsByDescendantsProtectedIsNotNullAndParentProtectedIsNotNull(): List<Iso3166SecondLevel<*,*>>
    fun findIso3166SecondLevelByCode3charAndLinkedBy_Code2char(@Param("code3char")code3char: Code3CharAlias,@Param("code2char") code2char: Code2CharAlias): Iso3166SecondLevel<*,*>?
}

interface EffectiveAddressesRepository: JpaRepository<EffectiveAddressInfo, Long> {
    fun findEffectiveAddressInfoByFilePath(filePath: FilePath): EffectiveAddressInfo?
}


