package ru.itis.addressdatabaseupdater.controller

import com.fasterxml.jackson.annotation.JsonView
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.coyote.http11.Constants.a
import org.apache.pulsar.shade.org.glassfish.hk2.utilities.BuilderHelper.link
import org.hibernate.mapping.OneToMany
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.validation.Validator
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RestController
import reactor.kotlin.core.publisher.zip
import ru.itis.addressdatabaseupdater.configs.properties.IsoInfo
import ru.itis.addressdatabaseupdater.configs.properties.OpenCagesInfo
import ru.itis.addressdatabaseupdater.dto.*

import ru.itis.addressdatabaseupdater.models.*
import ru.itis.addressdatabaseupdater.parsers.Country2LanguagesAdapter
import ru.itis.addressdatabaseupdater.parsers.OpenCagesDataParser
import ru.itis.addressdatabaseupdater.parsers.WikiIsoParser
import ru.itis.addressdatabaseupdater.services.CountriesService
import ru.itis.addressdatabaseupdater.services.LanguagesService
import ru.itis.addressdatabaseupdater.utils.annotations.CommonsUnsafe
import java.io.InputStream
import java.nio.file.Path
import kotlin.io.path.inputStream
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.random.Random
import ru.itis.addressdatabaseupdater.services.SubdivisionsService

import ru.itis.addressdatabaseupdater.parsers.StringToIsoToponym
import ru.itis.addressdatabaseupdater.parsers.StringToCode2Char
import ru.itis.addressdatabaseupdater.parsers.StringToLanguage
import ru.itis.addressdatabaseupdater.parsers.StringToCode3Char
import ru.itis.addressdatabaseupdater.services.SubdivisionsServiceImpl
import ru.itis.addressdatabaseupdater.views.IsoViews

class ValidatorException(message: String) : RuntimeException(message)

enum class SourcesContent {
    languages,
    countries,
    divisions,
    languagesToCountryCode,
}

enum class InternalSources(vararg val contents: SourcesContent) {
    wikiScrapped(SourcesContent.languages, SourcesContent.countries, SourcesContent.divisions), openCagesData(
        SourcesContent.languagesToCountryCode
    )
}

interface AdminController {
    fun isoUpdaterFromInternalSource(source: InternalSources, content: SourcesContent): Any
}

fun validate(validator: Validator, any: Any): Boolean = if (validator.validateObject(any).hasErrors()) false else true

@Throws(ValidatorException::class)
fun <A : Any> throwValidate(validator: Validator, any: A): A =
    if (validator.validateObject(any).hasErrors()) throw ValidatorException("validation error") else any

fun <A : Any> validateList(validator: Validator, list: Collection<A>) =
    if (list.map { validate(validator, list) }.filter { it == false }.size != 0) false else true

@RestController
class AdminControllerImpl(
    val langService: LanguagesService,
    val countriesService: CountriesService,
    val subdivisionsService: SubdivisionsService,
    val wikiIsoParser: WikiIsoParser,
    val openCagesDataParser: OpenCagesDataParser,
    val cagesAdapter: Country2LanguagesAdapter,
    val openCagesInfo: OpenCagesInfo,
    val isoInfo: IsoInfo,
    val validator: Validator,
    @Qualifier("YamlMapper")
    val yamlMapper: ObjectMapper,
    val adapter: Country2LanguagesAdapter,
) : AdminController {
    val regex2Char = Regex("""(?<=\w\w-)\w{2,2}|(?<=^)\w{2,2}(?!.*-)""")
    val regex3Char = Regex("""(?<=\w\w-)\w{1,3}|(?<=^)\w{1,3}(?!.*-)""")
    val regexToponym = Regex("""(?<!([\(\[] )|[\(\[])[^\(\)\[\] ]+( [^\(\)\[\] ]*)*(?!( [\)\]])|[\)\]])""")

    val stringToCode2Char: StringToCode2Char =
        { regex2Char.find(it)?.value?.let { Code2Char(it) } ?: throw ValidatorException("validation error") }
    val stringToCode3Char: StringToCode3Char =
        { regex3Char.find(it)?.value?.let { Code3Char(it) } ?: throw ValidatorException("validation error") }
    val stringToLanguage: StringToLanguage = { throwValidate(validator, Language(it)) }
    val stringToIsoToponym: StringToIsoToponym =
        { regexToponym.find(it)?.value?.let { IsoToponym(it) } ?: throw ValidatorException("validation error") }

    private inline fun <I : Iso> updateWith(
        folder: Path,
        regex: Regex,
        crossinline doIt: (InputStream) -> Iterable<I>
    ): Iterable<I> =
        folder.listDirectoryEntries().filter { regex.matches(it.fileName.name) }.map { doIt(it.toFile().inputStream()) }
            .flatten()

    @JsonView(IsoViews.FlatMapLanguage::class)
    @GetMapping("debug/lang")
    fun tempLanguages() = langService.findAll()

    @JsonView(IsoViews.FlatMapCountry::class)
    @GetMapping("debug/count")
    fun tempCountries() = countriesService.findAll()

    @JsonView(IsoViews.FlatMapSubdivisionNonRecursiveInternally::class)
    @GetMapping("debug/subdivisions")
    fun tempSubdivisions() = subdivisionsService.findAll()

    @JsonView(IsoViews.FlatMapSubdivisionRecursiveInternally::class)
    @GetMapping("debug/subdivisions-recursive")
    fun tempSubdivisionsRecursive() = subdivisionsService.findAll()

    @JsonView(IsoViews.FlatMapSubdivisionRecursiveInternally::class)
    @GetMapping("debug/descendants-null")
    fun tempSubdivisionByCode() = subdivisionsService.findIso3166SecondLevelsByDescendantsIsNull()

    @JsonView(IsoViews.GeneralRecursive::class)
    @GetMapping("debug/all")
    fun tempAll() = countriesService.findAll()

    @JsonView(IsoViews.FlatMapSubdivisionRecursiveInternally::class)
    @GetMapping("debug/parent-and-child")
    fun tempParntAndChild() = subdivisionsService.findIso3166SecondLevelsParentAndChild()

    @JsonView(IsoViews.FlatMapSubdivisionNonRecursiveInternally::class)
    @GetMapping("debug/subdivisions/{parentCode}-{childCode}")
    fun tempSubdivisionByCode(@PathVariable parentCode: String, @PathVariable childCode: String) =
        subdivisionsService.findIso3166SecondLevelByCountryCodeAndSubdivisionCode(
            stringToCode2Char(parentCode),
            stringToCode3Char(childCode)
        )


    @OptIn(CommonsUnsafe.UnsafeAs::class)
    @PutMapping("api/v.0-alpha/iso-updater/internal/{source}/{content}")
    override fun isoUpdaterFromInternalSource(
        @PathVariable source: InternalSources,
        @PathVariable content: SourcesContent
    ): Boolean {
        when (source) {
            InternalSources.wikiScrapped -> when (content) {
                SourcesContent.languages -> {
                    var temp = updateWith(
                        isoInfo.iso639.folder,
                        isoInfo.iso639.filter
                    )
                    { wikiIsoParser.parseCsvToIso639Level1(it, stringToCode2Char, stringToCode3Char, stringToLanguage) }
                    langService.saveAll(temp)
                    return true
                }

                SourcesContent.countries -> {
                    val temp = updateWith(
                        isoInfo.iso3166first.folder,
                        isoInfo.iso3166first.filter
                    ) {
                        wikiIsoParser.parseCsvToIso3166Level1(
                            it,
                            stringToCode2Char,
                            stringToCode3Char,
                            stringToIsoToponym
                        )
                    }
                    temp.forEach(countriesService::save)
                    return true
                }

                SourcesContent.divisions -> {
                    isoInfo.iso3166second.folder.listDirectoryEntries()
                        .filter { isoInfo.iso3166second.filter.matches(it.fileName.name) }
                        .map { isoInfo.iso3166second.countryCodeSelector.find(it.fileName.name)!!.value to it }
                        .groupBy { it.first }.mapValues { it.value.map { it.second } }
                        .mapValues { it: Map.Entry<String, List<Path>> ->
                            wikiIsoParser.parseCsvToIso3166Level2(
                                stringToCode3Char,
                                stringToIsoToponym,
                                it.value,
                                stringToCode2Char(it.key)
                            ).unzip()
                        }.map { (keyTop, value) ->
                            val (enrich, toponyms) = value.first to value.second

                            val toponymsFlat = toponyms.flatten().toMap()
                            val (manyToOne, oneToMany: Map<Code3Char, List<Code3Char>>) = enrich.filterNotNull()
                                .flatten().flatten()
                                .let { it.toMap() to it.groupBy { it.second }.mapValues { it.value.map { it.first } } }

                            fun recurse(parent: Pair<Code3Char, List<IsoToponym>>) : List<Pair<Code3Char, List<IsoToponym>>> {
                                val debug = oneToMany[parent.first]!!.let {
                                    val parents = it.filter { (oneToMany.containsKey(it)) }

                                    val childs = parents.map{recurse(it to toponymsFlat[it]!!)}.flatten()
                                    childs + (it).map { it to toponymsFlat[it]!! }
                                }

                                return debug
                            }

                            val (onlyParrents, lonelies) = if (oneToMany.isEmpty() && manyToOne.isEmpty()) null to toponymsFlat.toList()
                            else toponymsFlat.asSequence()
                                .groupBy { oneToMany.containsKey(it.key) && !manyToOne.containsKey(it.key) }[true]!!
                                .map { it.toPair() }
                                .let { it to toponymsFlat.toList() - (it.map { recurse(it) }.flatten() + it)}


                            val linkedBy: Iso3166FirstLevel =
                                countriesService.findIso3166FirstLevelByCode2Char(stringToCode2Char(keyTop))!!

                            if (lonelies.size != 0) {
                                val debuggg =  lonelies.map {
                                        Iso3166SecondLevelLonely(
                                            it.first,
                                            stringToIsoToponym(it.second.joinToString("::") { it.name }),
                                            linkedBy,
                                        )}.forEach{linkedBy.subdivisions.add(it)}
                            }

                            if (onlyParrents != null) {
                                val receipt =
                                    { it: Pair<Pair<Code3Char, List<IsoToponym>>, Iso3166FirstLevel> ->
                                        Iso3166SecondLevelOnlyParent(
                                            it.first.first,
                                            stringToIsoToponym(it.first.second.joinToString("::") { it.name }),
                                            it.second,
                                            mutableSetOf()
                                        )
                                    }


                                fun recursiveAdding(
                                    it: Code3Char,
                                    parent: Iso3166SecondLevel<*, HasChild>
                                ): Iso3166SecondLevel<*, *> {
                                    var parentt = when {
                                        it in oneToMany ->
                                            Iso3166SecondLevelParentNChild(
                                                it,
                                                stringToIsoToponym(toponymsFlat[it]!!.joinToString("::") { it.name }),
                                                linkedBy,
                                                parent,
                                                mutableSetOf()
                                            )

                                        it in manyToOne -> Iso3166SecondLevelOnlyChild(
                                            it,
                                            stringToIsoToponym(toponymsFlat[it]!!.joinToString("::") { it.name }),
                                            linkedBy,
                                            parent
                                        )
                                            .let {
                                                return it
                                            }

                                        else -> throw IllegalStateException("wtf")
                                    }
                                    var list = oneToMany[it]!!.map { recursiveAdding(it, parentt) }

                                    parentt.descendantsProtected!!.addAll(
                                        list as List<Iso3166SecondLevel<HasParent, *>>
                                    )


                                    return parentt
                                }

                                onlyParrents.map {
                                    var parent = receipt(it to linkedBy)
                                    if (oneToMany.containsKey(it.first)) {

                                        var list: List<Iso3166SecondLevel<*, *>> = oneToMany[it.first]!!.map {
                                            recursiveAdding(
                                                it,
                                                parent as Iso3166SecondLevel<*, HasChild>
                                            )
                                        }

                                        parent.descendantsProtected!!.addAll(
                                            list as List<Iso3166SecondLevel<HasParent, *>>
                                        )

                                        linkedBy.subdivisions.addAll(list)
                                        return@map parent
                                    } else {

                                        return@map parent
                                    }

                                }.let { linkedBy.subdivisions.addAll(it) }
                            }
                            countriesService.save(linkedBy)
                        }
                    return true
                }

                SourcesContent.languagesToCountryCode -> return false;
            }


            InternalSources.openCagesData -> when (content) {
                SourcesContent.languagesToCountryCode -> {
                    openCagesDataParser.parse(openCagesInfo.countryToLanguagePath.inputStream())
                        .map { cagesAdapter.adapt(it) }
                        .also {
                            if (!validateList(validator, it)) {
                                return false
                            }
                        }
                        .forEach { link(it) }
                    return true
                }

                else -> throw IllegalArgumentException("unsupported content: $content - in source: $source")
            }

            else -> throw IllegalArgumentException("unsupported source: $source")
        }
    }

    @CommonsUnsafe.UnsafeAs
    fun link(link: Country2LanguagesDTO) {
        var lastLang: String? = null
        try {
            val country = countriesService.findIso3166FirstLevelByCode2Char(stringToCode2Char(link.country))
                ?: throw IllegalArgumentException("no country")
            val languages =
                link.languages.map {
                    langService.findIso639FirstLevelByCode2Char(stringToCode2Char(adapter.adapt(it)))
                }.map {
                    it ?: throw NullPointerException("no language")
                }
            country.languages =
                languages.also { it: List<Iso639FirstLevel> -> it.forEach { it.countries.plus(country) } }
                    .toMutableSet()

            (country.languages)
            countriesService.save(country)

            return

        } catch (e: IllegalArgumentException) {
            //TODO
            println(" debug $lastLang")
            throw e
        }
    }

//    fun resulter(
//        states: MutableList<Pair<Code3Char, IsoToponym>>,
//        counties: MutableList<Pair<Code3Char, IsoToponym>>?,
//        linkedBy: Code2Char,
//        provider: Map<Code3Char, List<Code3Char>>,
//    ): Pair<MutableList<Iso3166SecondLevel>, MutableList<Iso3166SecondLevel>?> {
//        counties ?: return states.map { a ->
//            Iso3166SecondLevelLonely(
//                a.first,
//                a.second,
//                countriesService.findIso3166FirstLevelByCode2Char(linkedBy)!!,
//            ) as Iso3166SecondLevel
//        }.toMutableList() to null
//
//        val mapDescendant = counties.toMap()
//
//        val onlyParents = mutableListOf<Iso3166SecondLevel>()
//        val onlyChildren = mutableListOf<Iso3166SecondLevel>()
//
//        states.forEach { a ->
//            onlyParents.add(
//                Iso3166SecondLevelOnlyParent(
//                    a.first,
//                    a.second,
//                    countriesService.findIso3166FirstLevelByCode2Char(linkedBy)!!,
//                    mutableSetOf()
//                )
//            )
//        }
//
//        onlyParents.forEach { it ->
//            onlyChildren.addAll(
//                provider.get<Code3Char, List<Code3Char>>(it.code3char.fromAllias())!!
//                    .map<Code3Char, Iso3166SecondLevelOnlyChild> { b: Code3Char ->
//                        Iso3166SecondLevelOnlyChild(
//                            b,
//                            (mapDescendant.get<Code3Char, IsoToponym>(b)!!),
//                            countriesService.findIso3166FirstLevelByCode2Char(linkedBy)!!,
//                            it
//                        )
//                    })
//        }
//
//        return onlyParents to onlyChildren
//    }
}