package ru.itis.addressdatabaseupdater.controller

//import com.mapzen.jpostal.ExpanderOptions
//import org.springframework.web.bind.annotation.GetMapping
//import org.springframework.web.bind.annotation.PathVariable
//import org.springframework.web.bind.annotation.RestController
//import com.mapzen.jpostal.ParsedComponent
//import ru.itis.addressdatabaseupdater.models.Address
//import ru.itis.addressdatabaseupdater.services.AddressTransformerService


//interface AddressTransformationController {
//    fun parseTransient(address: String): Pair<Address, Array<ParsedComponent>>
//    fun expandTransient(address: String): String
//}
//
//@RestController
//class AddressTransformationControllerImpl(val service: AddressTransformerService): AddressTransformationController {
//    @GetMapping("api/v.0-alpha/address-parser/transient/{address}")
//    override fun parseTransient(@PathVariable address: String): Pair<Address, Array<ParsedComponent>> = service.parse(address)
//
//    @GetMapping("api/v.0-alpha/address-transformer/transient/{address}")
//    override fun expandTransient(@PathVariable address: String): String = service.expand(address)
//
////    @PostMapping("/")
////    fun post(@RequestBody message: Message) { service.save(message) }
//}