package ru.itis.addressdatabaseupdater.controller

import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController


@RestController
class Shutdown : ApplicationContextAware {
    private var context: ApplicationContext? = null

    @PostMapping("/shutdownContext")
    fun shutdownContext() {
        (context as ConfigurableApplicationContext?)!!.close()
    }


    @Throws(BeansException::class)
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        this.context = applicationContext
    }
}