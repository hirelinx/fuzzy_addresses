package ru.itis.addressdatabaseupdater.controller

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import ru.itis.addressdatabaseupdater.errors.IllegalArgumentHandler
import ru.itis.addressdatabaseupdater.errors.LanguagesRepositoryException
import ru.itis.addressdatabaseupdater.errors.OptimisticLockingFailureHandler
import ru.itis.addressdatabaseupdater.errors.SpringDataJdbcHandler

data class Error(val message: String, val error: String)

@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [LanguagesRepositoryException::class])
    protected fun handleLanguagesConflict(
        ex: RuntimeException, request: WebRequest
    ): ResponseEntity<Any>? {

        val (body, status) = when (ex) {
            is LanguagesRepositoryException -> {
                when (ex.carrier) {
                    is IllegalArgumentHandler -> Error(
                        ex.message ?: "invalid argument",
                        ex.carrier.exception.stackTraceToString()
                    ) to HttpStatus.BAD_REQUEST

                    is OptimisticLockingFailureHandler -> Error(
                        ex.message ?: "internal error",
                        ex.carrier.exception.stackTraceToString()
                    ) to HttpStatus.INTERNAL_SERVER_ERROR

                    is SpringDataJdbcHandler -> Error(
                        ex.message ?: "invalid argument",
                        ex.carrier.exception.stackTraceToString()
                    ) to HttpStatus.BAD_REQUEST
                }
            }

            else -> throw IllegalStateException("Unexpected error: $ex")
        }
        return handleExceptionInternal(
            ex, body,
            HttpHeaders(), status, request
        )
    }

//    @ExceptionHandler(value = [CountiesRepositoryException::class])
//    protected fun handleCountriesConflict(
//        ex: RuntimeException, request: WebRequest
//    ): ResponseEntity<Any>? {
//
//        val (body, status) = when (ex) {
//            is CountiesRepositoryException -> Error(
//                ex.message ?: "invalid argument",
//                ex.cause ?: throw IllegalStateException("Unexpected error: $ex")
//            ) to HttpStatus.BAD_REQUEST
//
//            else -> throw IllegalStateException("Unexpected error: $ex")
//        }
//        return handleExceptionInternal(
//            ex, body,
//            HttpHeaders(), status, request
//        )
//    }
}