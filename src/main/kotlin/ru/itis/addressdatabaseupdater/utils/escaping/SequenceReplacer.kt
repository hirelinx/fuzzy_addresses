package ru.itis.addressdatabaseupdater.utils.escaping


interface SequenceReplacer {
    fun replaceSequence(sequence: CharSequence): String
}

class SequenceNamedGroupReplacerImpl(
    val groupNames: Collection<String>,
    val groupNameToValueReplacer: (groupMap: Map<String, Pair<IntRange, String>>) -> Collection<Pair<IntRange, String>>,
    val regex: Regex
) : SequenceReplacer {
    constructor (
        groupToGroupReplacer: Map<String, (groupValue: String) -> String>,
        regex: Regex,
    ) : this(
        groupToGroupReplacer.keys,
        { groupMap ->
            groupMap.entries.map {
                it.value.first to (groupToGroupReplacer[it.key]?.invoke(it.value.second) ?: it.value.second)
            }
        },
        regex,
    )

    override fun replaceSequence(sequence: CharSequence): String {
        val matches = regex.findAll(sequence)

        if (matches.none()) {
            return sequence.toString()
        }

        val resultBuilder: StringBuilder = StringBuilder()

        for (match in matches) {
            resultBuilder.append(sequence.subSequence(0, match.range.first))
            val groupMap: MutableMap<String, Pair<IntRange, String>> = mutableMapOf()

            for (groupName in groupNames) {
                val subgroup = match.groups[groupName]
                if (subgroup != null && !subgroup.range.isEmpty()) {
                    groupMap[groupName] = subgroup.range to subgroup.value
                }
            }
            val replacements: Iterator<Pair<IntRange, String>> =
                groupNameToValueReplacer(groupMap).sortedBy { it.first.first }.iterator()


//            var shift = 0
            var currentReplacement: Pair<IntRange, String>? =
                if (replacements.hasNext()) replacements.next() else null

            var i = match.range.first
            while (true) {
                if (currentReplacement == null) {
                    resultBuilder.append(sequence.subSequence(i, sequence.length))
                    break
                }
                resultBuilder.append(sequence.subSequence(i, currentReplacement.first.first))
                resultBuilder.append(currentReplacement.second)
                i = currentReplacement.first.last + 1
//                shift += currentReplacement.first.last - currentReplacement.first.first + 1 - currentReplacement.second.length
                currentReplacement = if (replacements.hasNext()) replacements.next() else null
            }
        }
        return resultBuilder.toString()
    }
}
