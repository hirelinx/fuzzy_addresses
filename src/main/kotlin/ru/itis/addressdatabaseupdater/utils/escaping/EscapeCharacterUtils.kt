package ru.itis.addressdatabaseupdater.utils.escaping

import java.util.regex.PatternSyntaxException
import kotlin.jvm.Throws

@Throws(IllegalArgumentException::class, PatternSyntaxException::class)
fun replaceEscapeCharsAndVariables(
    varNameToString: (String) -> String?,
    variableSelector: String = """(?<varselectorbef>\{ ?)(?<var>\w*)(?<varselectoraft> ?\})""",
    selectorEscaper: String = "\\\\",
    string: String,
    escapeOdd: Boolean = true,
    onVarConvertedToNullCall: (String) -> String = { throw IllegalArgumentException("no such var name as \"$it\"") }
): String {
    val replacer = SequenceNamedGroupReplacerImpl(
        groupNames = listOf(
            "evenbeftrue",
            "evenbeffalse",
            "evenafttrue",
            "evenaftfalse",
            "varselectorbef",
            "var",
            "varselectoraft",
        ),
        groupNameToValueReplacer = {
            val result = mutableListOf<Pair<IntRange, String>>()
            var escapeVar = false

            val internal: (label: String, evenBool: Boolean) -> Unit = { label, evenBool ->
                val cur = it[label]
                if (cur != null) {
                    result.add(cur.first to String(CharArray((cur.second.length - if (evenBool) 0 else 1) / 2) { '\\' }))
                    if (escapeOdd && !evenBool || !escapeOdd && evenBool) {
                        escapeVar = true
                    }
                }
            }

            internal("evenbeffalse", false)
            internal("evenbeftrue", true)
            internal("evenbaffalse", false)
            internal("evenafttrue", true)
            var cur = it["varselectorbef"]
            if (cur != null) {
                result.add(cur.first to "")
            }
            cur = it["varselectoraft"]
            if (cur != null) {
                result.add(cur.first to "")
            }
            cur = it["var"]
            if (cur != null) {
                result.add(
                    cur.first to if (escapeVar) cur.second else varNameToString(cur.second) ?: onVarConvertedToNullCall(
                        cur.second
                    )
                )
            }
            result
        },
        regex = Regex("(^|(?<!${selectorEscaper})(?<evenbeftrue>${selectorEscaper}${selectorEscaper}|(${selectorEscaper}{2})+)|(?<evenbeffalse>${selectorEscaper}*))${variableSelector}(${'$'}|(?<evenafttrue>${selectorEscaper}${selectorEscaper}|(${selectorEscaper}{2})*)(?!${selectorEscaper})|(?<evenaftfalse>${selectorEscaper}*))")
    )
    return replacer.replaceSequence(string)
}