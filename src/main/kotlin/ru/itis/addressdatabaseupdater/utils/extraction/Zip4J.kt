package ru.itis.addressdatabaseupdater.utils.extraction

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.exception.ZipException
import net.lingala.zip4j.model.FileHeader
import net.lingala.zip4j.model.UnzipParameters
import ru.itis.addressdatabaseupdater.utils.data.Quadruple
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.jvm.Throws

object Zip4JProvider :
    FormatProvider<ArchiveFormats.Zip, Zip4JArchiveExceptionHolder, ZipException, Zip4JInformation, FileHeader, Zip4JArchivedFileDescriptorHolder, Zip4JFormatProcessor> {
    override fun getDescriptor(): FormatProvider.Descriptor<ArchiveFormats.Zip, Zip4JArchiveExceptionHolder, ZipException, Zip4JInformation, FileHeader, Zip4JArchivedFileDescriptorHolder, Zip4JFormatProcessor> =
        FormatProvider.Descriptor(
            ArchiveFormats.Zip, Zip4JArchiveExceptionHolder(ZipException("")), ZipException(""), Zip4JInformation(
                UnzipParameters()
            ), FileHeader(), Zip4JArchivedFileDescriptorHolder(FileHeader()), Zip4JFormatProcessor()
        )


}

class Zip4JInformation(val unzipParameters: UnzipParameters) :
    ArchiveInformation<Zip4JArchiveExceptionHolder, ZipException> {
    var zipFile: ZipFile? = null

    override var archivePath: Path?
        get() = zipFile?.file?.toPath()
        @Throws(ZipException::class)
        set(value) {
            zipFile = value?.let {
                finalizeInfoPathWise()
                ZipFile(it.toFile())
            }
        }

    @Throws(ZipException::class)
    override fun finalizeInfoPathWise(nullification: Boolean) {
        try {
            zipFile?.close()
        } catch (e: IOException) {
            throw ZipException(e)
        }
        if (nullification) {
            zipFile = null
        }
    }

}

@JvmInline
value class Zip4JArchivedFileDescriptorHolder(override val holden: FileHeader) : ArchivedFileDescriptor<FileHeader>

class Zip4JArchiveExceptionHolder(override var exception: ZipException) :
    ArchiveExceptionHolder<ZipException>, ArchiveException(exception)

const val ITERATING_CHANNEL_BUFFER_SIZE = 2


class Zip4JFormatProcessor :
    FormatProcessor<Zip4JArchiveExceptionHolder, ZipException, Zip4JInformation, FileHeader, Zip4JArchivedFileDescriptorHolder> {
    class Zip4JFormatOperatorProcessorException : ZipException {
        constructor(message: String?) : super(message)
        constructor(rootException: Exception?) : super(rootException)
        constructor(message: String?, rootException: Exception?) : super(message, rootException)
        constructor(message: String?, type: Type?) : super(message, type)
        constructor(message: String?, throwable: Throwable?, type: Type?) : super(message, throwable, type)
    }


    @Throws(ZipException::class)
    @OptIn(ExperimentalCoroutinesApi::class)
    override fun CoroutineScope.iterateArchive(archive: Zip4JInformation): ReceiveChannel<Zip4JArchivedFileDescriptorHolder> =
        produce(capacity = ITERATING_CHANNEL_BUFFER_SIZE) {
            val fileHeaders: MutableList<FileHeader> =
                archive.zipFile?.fileHeaders ?: throw Zip4JFormatOperatorProcessorException("unset path name")
            fileHeaders.forEach { fileHeader: FileHeader ->
                send(Zip4JArchivedFileDescriptorHolder(fileHeader))
            }
        }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun CoroutineScope.filterFileNames(
        filterOrSend: (Zip4JArchivedFileDescriptorHolder) -> Boolean,
        fileHeaders: ReceiveChannel<Zip4JArchivedFileDescriptorHolder>
    ) = produce {
        for (fileHeader in fileHeaders) {
            if (!filterOrSend(fileHeader)) {
                send(fileHeader)
            }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun CoroutineScope.getExtractionPathAndName(
        getPath: (Zip4JArchivedFileDescriptorHolder) -> Pair<Path, String>,
        fileHeaders: ReceiveChannel<Zip4JArchivedFileDescriptorHolder>
    ) = produce {
        for (fileHeader in fileHeaders) {
            val (path, name) = getPath(fileHeader)
            send(Triple(fileHeader, path, name))
        }
    }

    @Throws(ZipException::class)
    @OptIn(ExperimentalCoroutinesApi::class)
    override fun CoroutineScope.extractArchive(
        archive: Zip4JInformation,
        fileHeadersAndExtractionPathAndNewName: ReceiveChannel<Triple<Zip4JArchivedFileDescriptorHolder, Path, String>>
    ) = produce {
        for ((fileHeader, extractionPath, newName) in fileHeadersAndExtractionPathAndNewName) {
            try {
                archive.zipFile?.extractFile(
                    fileHeader.holden,
                    extractionPath.toString(),
                    newName,
                    archive.unzipParameters
                )?.let { send(Quadruple(
                    fileHeader,
                    Path(extractionPath.toString(), newName),
                    true,
                    null
                )) } ?: send(
                    Quadruple(
                        fileHeader,
                        Path(extractionPath.toString(), newName),
                        false,
                        Zip4JFormatOperatorProcessorException("unset path name")
                    )
                )
            } catch (e: ZipException) {
                send(Quadruple(fileHeader, Path(extractionPath.toString(), newName), false, e))
            }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun CoroutineScope.getFileStreams(
        archive: Zip4JInformation,
        fileHeaders: ReceiveChannel<Zip4JArchivedFileDescriptorHolder>
    ): ReceiveChannel<Pair<Zip4JArchivedFileDescriptorHolder, InputStream>> = produce {
        for (fh in fileHeaders) {
            send(Pair(fh,archive.zipFile?.getInputStream(fh.holden)?: throw UnsupportedOperationException("zip file is null")))
        }

    }
}