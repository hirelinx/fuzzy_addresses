package ru.itis.addressdatabaseupdater.utils.extraction

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import ru.itis.addressdatabaseupdater.utils.data.QuadrupleAccessor
import java.io.InputStream
import java.nio.file.Path


interface ArchiveFormat {
    override fun toString(): String
}

class ArchiveFormats {
    object Zip : ArchiveFormat {
        override fun toString() = "ZIP"
    }
}

interface FormatProvider<A : ArchiveFormat, E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>> where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
    data class Descriptor<A : ArchiveFormat, E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>>(
        val archiveFormat: A,
        val exception: E,
        val exceptionInner: EI,
        val information: I,
        val fileDescriptor: H,
        val fileDescriptorHolder: FD,
        val processor: FP
    ) where E : ArchiveException, E : ArchiveExceptionHolder<EI>

    fun getDescriptor(): Descriptor<A, E, EI, I, H, FD, FP>
}

interface ArchiveInformation<E, EI : Exception> where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
    @set:Throws(ArchiveException::class)
    var archivePath: Path?

    @Throws(ArchiveException::class)
    fun finalizeInfoPathWise(nullification: Boolean = true)
}

interface ArchivedFileDescriptor<H> {
    val holden: H
}


open class ArchiveException : Exception {
    @SuppressWarnings("unused")
    constructor() : super()
    constructor(cause: Throwable) : super(cause)
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
    constructor(message: String, cause: Throwable, enableSuppression: Boolean, writableStackTrace: Boolean) : super(
        message,
        cause,
        enableSuppression,
        writableStackTrace
    )
}

class ExternalException : Exception {
    constructor(cause: Exception) : super(cause) {
        val externalException = cause
    }

    constructor(message: String, cause: Exception) : super(message, cause) {
        val externalException = cause
    }

    constructor(message: String, cause: Exception, enableSuppression: Boolean, writableStackTrace: Boolean) : super(
        message,
        cause,
        enableSuppression,
        writableStackTrace
    ) {
        val externalException = cause
    }
}

interface ArchiveExceptionHolder<E : Exception> {
    var exception: E
}

interface FormatProcessor<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>>
        where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
    @Throws(ArchiveException::class, UnsupportedOperationException::class)
    fun CoroutineScope.iterateArchive(archive: I): ReceiveChannel<FD>

    fun CoroutineScope.filterFileNames(
        filterOrSend: (FD) -> Boolean,
        fileHeaders: ReceiveChannel<FD>
    ): ReceiveChannel<FD>

    @Throws(ArchiveException::class, UnsupportedOperationException::class)
    fun CoroutineScope.getFileStreams(
        archive: I,
        fileHeaders: ReceiveChannel<FD>
    ): ReceiveChannel<Pair<FD, InputStream>>

    fun CoroutineScope.getExtractionPathAndName(
        getPath: (FD) -> Pair<Path, String>,
        fileHeaders: ReceiveChannel<FD>
    ): ReceiveChannel<Triple<FD, Path, String>>


    @Throws(ArchiveException::class, UnsupportedOperationException::class)
    fun CoroutineScope.extractArchive(
        archive: I,
        fileHeadersAndExtractionPathAndNewName: ReceiveChannel<Triple<FD, Path, String>>,
    ): ReceiveChannel<QuadrupleAccessor<FD, Path, Boolean, EI?>>
}


interface Extractor {
    @Throws(IllegalArgumentException::class, ArchiveException::class, UnsupportedOperationException::class)
    fun extract(archivePath: Path)
}

interface FileStreamer<FID> {
    @Throws(IllegalArgumentException::class, ArchiveException::class, UnsupportedOperationException::class)
    suspend fun getFileStreams(
        archivePath: Path,
        consumer: suspend (ReceiveChannel<Pair<FID, InputStream>>) -> Unit
    )
}

data class FileStreamState<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FID>(
    val iterateState: IterateState<E, EI, I, H, FD>,
    val iteratedInterceptor: suspend (ReceiveChannel<FD>) -> ReceiveChannel<FD> = { it },
    val streamsNewURIProvider: (I, FD) -> FID,
    val streamsResolverInterceptor: suspend (ReceiveChannel<Pair<FID, InputStream>>) -> ReceiveChannel<Pair<FID, InputStream>> = { it },
    val autoClose: Boolean = true
) where E : ArchiveException, E : ArchiveExceptionHolder<EI>


open class ArchivedFileStreamer<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>, FID>(
    var fileStreamState: FileStreamState<E, EI, I, H, FD, FID>,
    processor: FP
) : FileStreamer<FID>, ArchiveIterator<E, EI, I, H, FD, FP>(
    fileStreamState.iterateState,
    processor
) where E : ArchiveException, E : ArchiveExceptionHolder<EI> {

    companion object {
        fun <A : ArchiveFormat, E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>, FID>
                instance(
            descriptor: FormatProvider.Descriptor<A, E, EI, I, H, FD, FP>,
            fileStreamState: FileStreamState<E, EI, I, H, FD, FID>
        ): ArchivedFileStreamer<E, EI, I, H, FD, FP, FID> where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
            return ArchivedFileStreamer(
                processor = descriptor.processor,
                fileStreamState = fileStreamState
            )
        }
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    fun CoroutineScope.URIProvider(
        information: I,
        streamsNewURIProvider: (I, FD) -> FID,
        channel: ReceiveChannel<Pair<FD, InputStream>>
    ) = produce {
        for ((fd, inputStream) in channel) {
            send(Pair(streamsNewURIProvider(information, fd), inputStream))
        }
    }

    @Throws(IllegalArgumentException::class, ArchiveException::class, UnsupportedOperationException::class)
    suspend fun FileStreamState<E, EI, I, H, FD, FID>.getFileStreams(
        consumer: suspend (ReceiveChannel<Pair<FID, InputStream>>) -> Unit,
        channel: ReceiveChannel<FD>
    ): Unit = coroutineScope {
        with(processor) {
            with(iterateState) {
                try {
                    consumer(
                        streamsResolverInterceptor(
                            URIProvider(
                                archiveInformation,
                                streamsNewURIProvider,
                                getFileStreams(archiveInformation, iteratedInterceptor(channel))
                            )
                        )
                    )
                } catch (e: ArchiveException) {
                    throw ArchiveException("Exception while archive reading: ", e)
                } catch (e: UnsupportedOperationException) {
                    throw ArchiveException("Unsupported by processor operation: ", e)
                } catch (e: Exception) {
                    throw ExternalException("Interrupted by external exception", e)
                }
            }
        }
    }

    suspend fun FileStreamState<E, EI, I, H, FD, FID>.getFileStreams(
        archivePath: Path,
        consumer: suspend (ReceiveChannel<Pair<FID, InputStream>>) -> Unit
    ): Unit = coroutineScope {
        try {
            iterateState.iterate(archivePath) {
                getFileStreams(consumer, it)
            }
        } catch (e: ArchiveException) {
            throw ArchiveException("Exception while archive reading: ", e)
        } catch (e: UnsupportedOperationException) {
            throw ArchiveException("Unsupported by processor operation: ", e)
        } catch (e: Exception) {
            throw ExternalException("Interrupted by external exception", e)
        }
    }

    override suspend fun getFileStreams(
        archivePath: Path,
        consumer: suspend (ReceiveChannel<Pair<FID, InputStream>>) -> Unit
    ): Unit = coroutineScope {
        with(fileStreamState) {
            getFileStreams(archivePath, consumer)
        }
    }

}

data class IterateState<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>>(
    val iterateInterceptor: suspend (ReceiveChannel<FD>) -> ReceiveChannel<FD> = { it },
    val filesFilter: (FD) -> Boolean = { false },
    val filterInterceptor: suspend (ReceiveChannel<FD>) -> ReceiveChannel<FD> = { it },
    val archiveInformation: I,
) where E : ArchiveException, E : ArchiveExceptionHolder<EI>


open class ArchiveIterator<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>>(
    var iterateState: IterateState<E, EI, I, H, FD>,
    val processor: FP,
) where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
    companion object {
        fun <A : ArchiveFormat, E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>>
                instance(
            descriptor: FormatProvider.Descriptor<A, E, EI, I, H, FD, FP>,
            iterateState: IterateState<E, EI, I, H, FD>
        ): ArchiveIterator<E, EI, I, H, FD, FP> where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
            return ArchiveIterator(processor = descriptor.processor, iterateState = iterateState)
        }
    }

    suspend fun IterateState<E, EI, I, H, FD>.iterate(
        archivePath: Path,
        consumer: suspend (ReceiveChannel<FD>) -> Unit
    ): Unit =
        coroutineScope {
            with(processor) {
                try {
                    try {
                        archiveInformation.archivePath = archivePath
                    } catch (e: ArchiveException) {
                        throw ArchiveException("Bad archive file provided: ", e)
                    }
                    try {
                        consumer(
                            filterInterceptor(
                                filterFileNames(
                                    filesFilter,
                                    iterateInterceptor(iterateArchive(archiveInformation))
                                )
                            )
                        )
                    } catch (e: ArchiveException) {
                        throw ArchiveException("Exception while archive reading: ", e)
                    } catch (e: UnsupportedOperationException) {
                        throw ArchiveException("Unsupported by processor operation: ", e)
                    } catch (e: Exception) {
                        throw ExternalException("Interrupted by external exception", e)
                    }
                } finally {
                    archiveInformation.finalizeInfoPathWise()
                }
            }
        }
}


data class ExtractState<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>>(
    val iterateState: IterateState<E, EI, I, H, FD>,
    val iteratedInterceptor: suspend (ReceiveChannel<FD>) -> ReceiveChannel<FD> = { it },
    val extractionPathAndNewNameProvider: (FD) -> Pair<Path, String>,
    val extractionPathResolverInterceptor: suspend (ReceiveChannel<Triple<FD, Path, String>>) -> ReceiveChannel<Triple<FD, Path, String>> = { it },
    val extractionSuccessConsumer: suspend (ReceiveChannel<QuadrupleAccessor<FD, Path, Boolean, EI?>>) -> Unit = { },
) where E : ArchiveException, E : ArchiveExceptionHolder<EI>

open class ArchiveExtractor<E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>>(
    var extractState: ExtractState<E, EI, I, H, FD>,
    processor: FP,
) : Extractor, ArchiveIterator<E, EI, I, H, FD, FP>(
    extractState.iterateState,
    processor
) where E : ArchiveException, E : ArchiveExceptionHolder<EI> {

    companion object {
        fun <A : ArchiveFormat, E, EI : Exception, I : ArchiveInformation<E, EI>, H, FD : ArchivedFileDescriptor<H>, FP : FormatProcessor<E, EI, I, H, FD>>
                instance(
            extractState: ExtractState<E, EI, I, H, FD>,
            descriptor: FormatProvider.Descriptor<A, E, EI, I, H, FD, FP>
        ): ArchiveExtractor<E, EI, I, H, FD, FP> where E : ArchiveException, E : ArchiveExceptionHolder<EI> {
            return ArchiveExtractor(extractState = extractState, processor = descriptor.processor)
        }
    }

    suspend fun ExtractState<E, EI, I, H, FD>.extract(fileDescriptors: ReceiveChannel<FD>): Unit =
        coroutineScope {
            with(processor) {
                with(iterateState) {
                    try {
                        extractionSuccessConsumer(
                            extractArchive(
                                archiveInformation,
                                extractionPathResolverInterceptor(
                                    getExtractionPathAndName(
                                        extractionPathAndNewNameProvider,
                                        iteratedInterceptor(fileDescriptors)
                                    )
                                )

                            )
                        )
                    } catch (e: ArchiveException) {
                        throw ArchiveException("Exception while archive reading: ", e)
                    } catch (e: UnsupportedOperationException) {
                        throw ArchiveException("Unsupported by processor operation: ", e)
                    } catch (e: Exception) {
                        throw ExternalException("Interrupted by external exception", e)
                    }
                }
            }
        }

    fun ExtractState<E, EI, I, H, FD>.extract(archivePath: Path): Unit = runBlocking {
        try {
            iterateState.iterate(archivePath) {
                extract(it)
            }
        } catch (e: ArchiveException) {
            throw ArchiveException("Exception while archive reading: ", e)
        } catch (e: UnsupportedOperationException) {
            throw ArchiveException("Unsupported by processor operation: ", e)
        } catch (e: Exception) {
            throw ExternalException("Interrupted by external exception", e)
        }
    }

    override fun extract(archivePath: Path) =
        this.extractState.extract(archivePath)

}