package ru.itis.addressdatabaseupdater.utils.annotations

import kotlin.annotation.AnnotationTarget.FUNCTION

@JvmInline
value class UnsafeReason(val description: String)

internal object CommonsUnsafe {
    @RequiresOptIn(level = RequiresOptIn.Level.ERROR)
    @Retention(AnnotationRetention.BINARY)
    @Target(
        FUNCTION,
    )
    annotation class UnsafeAs {
        companion object {
            val reason: UnsafeReason = UnsafeReason("unsafe `this as T`")
        }
    }
    @RequiresOptIn(level = RequiresOptIn.Level.ERROR)
    @Retention(AnnotationRetention.BINARY)
    @Target(
        FUNCTION,
    )
    annotation class AssumeNoArgsConstructor {
        companion object {
            val reason: UnsafeReason = UnsafeReason("unsafe `this as T`")
        }
    }

    @RequiresOptIn(level = RequiresOptIn.Level.ERROR)
    @Retention(AnnotationRetention.BINARY)
    @Target(
        FUNCTION,
    )
    annotation class ByteBuddy {
        companion object {
            val reason: UnsafeReason = UnsafeReason("unsafe `this as T`")
        }
    }
}