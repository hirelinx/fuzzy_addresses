package ru.itis.addressdatabaseupdater.utils.commons

interface Stringer {
    override fun toString(): String
}

interface HashCoder {
    override fun hashCode(): Int
}

interface Equaler {
    override fun equals(other: Any?): Boolean
}