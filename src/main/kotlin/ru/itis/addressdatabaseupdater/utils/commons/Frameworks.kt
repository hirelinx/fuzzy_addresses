package ru.itis.addressdatabaseupdater.utils.commons

import org.apache.poi.ss.formula.functions.T
import org.h2.jdbc.*
import java.sql.SQLException
import kotlin.reflect.KClass

import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface Slf4j {
    val log: Logger
}

inline fun <reified E: Slf4j> E.getLogger(): Logger = LoggerFactory.getLogger(E::class.java)

//fun debug() {
//    val getHibernateException = getHibernateException(JdbcSQLSyntaxErrorException("", "", "", 0, IllegalArgumentException(), "")) { it: JdbcSQLSyntaxErrorException ->
//        it
//    }
//}

data class Resolved(
    val jdbcSQLDataException: JdbcSQLDataException? = null,
    val jdbcSQLException: JdbcSQLException? = null,
    val jdbcSQLFeatureNotSupportedException: JdbcSQLFeatureNotSupportedException? = null,
    val jdbcSQLIntegrityConstraintViolationException: JdbcSQLIntegrityConstraintViolationException? = null,
    val jdbcSQLInvalidAuthorizationSpecException: JdbcSQLInvalidAuthorizationSpecException? = null,
    val jdbcSQLNonTransientConnectionException: JdbcSQLNonTransientConnectionException? = null,
    val jdbcSQLNonTransientException: JdbcSQLNonTransientException? = null,
    val jdbcSQLSyntaxErrorException: JdbcSQLSyntaxErrorException? = null,
    val jdbcSQLTimeoutException: JdbcSQLTimeoutException? = null,
    val jdbcSQLTransactionRollbackException: JdbcSQLTransactionRollbackException? = null,
    val jdbcSQLTransientException: JdbcSQLTransientException? = null
)

interface HibernateSupport {
    fun getHibernateException(exception: SQLException): Resolved? = getHibernateResolved(exception)
}

object HibernateSupporter : HibernateSupport


fun getHibernateResolved(exception: SQLException): Resolved? {
    val temp = exception as? JdbcException ?: return null
    return when (temp) {
        is JdbcSQLDataException -> Resolved(jdbcSQLDataException = temp)
        is JdbcSQLException -> Resolved(jdbcSQLException = temp)
        is JdbcSQLFeatureNotSupportedException -> Resolved(jdbcSQLFeatureNotSupportedException = temp)
        is JdbcSQLIntegrityConstraintViolationException -> Resolved(jdbcSQLIntegrityConstraintViolationException = temp)
        is JdbcSQLInvalidAuthorizationSpecException -> Resolved(jdbcSQLInvalidAuthorizationSpecException = temp)
        is JdbcSQLNonTransientConnectionException -> Resolved(jdbcSQLNonTransientConnectionException = temp)
        is JdbcSQLNonTransientException -> Resolved(jdbcSQLNonTransientException = temp)
        is JdbcSQLSyntaxErrorException -> Resolved(jdbcSQLSyntaxErrorException = temp)
        is JdbcSQLTimeoutException -> Resolved(jdbcSQLTimeoutException = temp)
        is JdbcSQLTransactionRollbackException -> Resolved(jdbcSQLTransactionRollbackException = temp)
        is JdbcSQLTransientException -> Resolved(jdbcSQLTransientException = temp)
        else -> null
    }
}



inline fun <reified C> getExclusiveHibernateException(exception: SQLException): KClass<out C>?
        where C : SQLException, C : JdbcException {
    val temp = exception as? JdbcException ?: return null
    return when (temp) {
        is JdbcSQLDataException -> (temp as C)::class
        is JdbcSQLException -> (temp as C)::class
        is JdbcSQLFeatureNotSupportedException -> (temp as C)::class
        is JdbcSQLIntegrityConstraintViolationException -> (temp as C)::class
        is JdbcSQLInvalidAuthorizationSpecException -> (temp as C)::class
        is JdbcSQLNonTransientConnectionException -> (temp as C)::class
        is JdbcSQLNonTransientException -> (temp as C)::class
        is JdbcSQLSyntaxErrorException -> (temp as C)::class
        is JdbcSQLTimeoutException -> (temp as C)::class
        is JdbcSQLTransactionRollbackException -> (temp as C)::class
        is JdbcSQLTransientException -> (temp as C)::class
        else -> null
    }
}


inline fun <reified C, T> getHibernateExclusiveHandler(exception: SQLException, crossinline handler: (C) -> T): T?
        where C : SQLException, C : JdbcException {
    val temp = exception as? JdbcException ?: return null
    return when (temp) {
        is JdbcSQLDataException -> handler(temp as C)
        is JdbcSQLException -> handler(temp as C)
        is JdbcSQLFeatureNotSupportedException -> handler(temp as C)
        is JdbcSQLIntegrityConstraintViolationException -> handler(temp as C)
        is JdbcSQLInvalidAuthorizationSpecException -> handler(temp as C)
        is JdbcSQLNonTransientConnectionException -> handler(temp as C)
        is JdbcSQLNonTransientException -> handler(temp as C)
        is JdbcSQLSyntaxErrorException -> handler(temp as C)
        is JdbcSQLTimeoutException -> handler(temp as C)
        is JdbcSQLTransactionRollbackException -> handler(temp as C)
        is JdbcSQLTransientException -> handler(temp as C)
        else -> null
    }
}