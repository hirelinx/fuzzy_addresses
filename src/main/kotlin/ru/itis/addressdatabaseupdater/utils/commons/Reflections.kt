package ru.itis.addressdatabaseupdater.utils.commons


import ru.itis.addressdatabaseupdater.utils.annotations.CommonsUnsafe
import kotlin.reflect.KClass
import kotlin.reflect.cast
import kotlin.reflect.full.createInstance

@CommonsUnsafe.UnsafeAs
inline fun <reified T> Any.unsafeAs() = this as T
@CommonsUnsafe.UnsafeAs
fun <T : Any> KClass<T>.unsafeAs(value: Any): T = this.cast(value)

//object DullProxy: MethodInterceptor {
//    override fun intercept(obj: Any?, method: Method?, args: Array<out Any>?, proxy: MethodProxy?): Any? {
//        return null
//    }
//}

object LightWeighter{
    val cache: MutableMap<KClass<*>, Any> = mutableMapOf()

    @CommonsUnsafe.UnsafeAs
    inline fun <reified E: Throwable> getThrowable() : E = (cache[E::class]?:
    E::class.createInstance()
        .also { cache[E::class] = it }).unsafeAs<E>()

//    @CommonsUnsafe.UnsafeAs
//    inline fun <reified E: Throwable> getThrowable() : E = (cash[E::class]?:
//    ByteBuddy()
//        .subclass(E::class.java)
//        .method (ElementMatchers.named("fillInStackTrace")).intercept(StubMethod.INSTANCE)
//        .make()
//        .load(E::class.java.classLoader, ClassLoadingStrategy.Default.INJECTION)
//        .loaded.kotlin.createInstance()
//    .also { cash[E::class] = it }).unsafeAs<E>()
}
