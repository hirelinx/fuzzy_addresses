package ru.itis.addressdatabaseupdater.utils.commons


//infix fun<A: Any> A.swap(second: A): Pair<A, A> = Pair(second, this)

interface InternallyOccurred

interface ExceptionCarrier<E : java.lang.Exception>: ScopeProvider3<E, InternallyOccurred, ExceptionCarrier<E>> {
    val exception: E
}



interface ExceptionRethrower<I: InternallyOccurred> {
    fun new(carrier: I, message: String): RuntimeException
    fun new(
        carrier: I,
    ): RuntimeException
//    @CommonsUnsafe.UnsafeAs
//    fun <E: Exception,C> getHandler(kClassE: KClass<E>, kClassC: KClass<C>) : ((e: E) ->  C)? where C : InternallyOccurred, C : ExceptionCarrier<E>
}

//@CommonsUnsafe.UnsafeAs
//inline fun <reified E:Exception, I: InternallyOccurred, reified C> ExceptionRethrower<I>.getHandler(scope: Scope3<in E,in I,in C>) : ((e: E) -> C)?  where C : InternallyOccurred, C : ExceptionCarrier<E> = getHandler(E::class, C::class)
//
//@CommonsUnsafe.UnsafeAs
//inline fun <reified E:Exception, I: InternallyOccurred, reified C> ExceptionRethrower<I>.getHandler(scope: Scope3<in E,in I,in C>, message: String) : ((e: E) -> C)?  where C : InternallyOccurred, C : ExceptionCarrier<E> = getHandler(E::class, C::class)

inline fun <A, C> ExceptionRethrower<C>.throwsAncestor(
    ancestor: ScopeProvider<A>,
    message: String,
    crossinline carrier: (A) -> C
): (e: A) -> Nothing  where A : Exception, C: InternallyOccurred = { it ->
    throw new(carrier(it), message)
}

inline fun <A , C> ExceptionRethrower<C>.throwsAncestor(
    ancestor: ScopeProvider<A>,
    crossinline carrier: (A) -> C
): (e: A) -> Nothing  where A : Exception, C: InternallyOccurred = { it ->
    throw new(carrier(it))
}


inline fun <E : Exception, C> ExceptionRethrower<C>.throws(
    message: String,
    crossinline carrier: (E) -> C
): (e: E) -> Nothing  where C : InternallyOccurred = { it ->
    throw new(carrier(it), message)
}

inline fun <E : Exception, C> ExceptionRethrower<C>.throws(
    crossinline carrier: (E) -> C
): (e: E) -> Nothing where C : InternallyOccurred = { it ->
    throw new(carrier(it))
}

