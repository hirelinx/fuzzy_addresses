package ru.itis.addressdatabaseupdater.utils.commons

import org.apache.poi.ss.formula.functions.T

interface ScopeProvider<T> {
    fun getScope(): Scope<T> {
        return Scope()
    }
}

interface ScopeProvider2<T1, T2> {
    fun getScope(): Scope2<T1, T2> {
        return Scope2()
    }
}


interface ScopeProvider3<T1, T2, T3> {
    fun getScope(): Scope3<T1, T2, T3> {
        return Scope3()
    }
}


class Scope<T>: ScopeProvider<T>

class Scope2<T1, T2>: ScopeProvider2<T1,T2>

class Scope3<T1, T2, T3>: ScopeProvider3<T1, T2, T3>