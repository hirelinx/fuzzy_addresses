@file:Suppress("unused")

package ru.itis.addressdatabaseupdater.utils.commons

import org.apache.poi.ss.formula.functions.T
import kotlin.reflect.full.isSuperclassOf

object SmartTry

inline fun <T> smartTry(block: SmartTry.() -> T) = with(SmartTry) { block() }

//inline fun <T> SmartTry.vararg(block: Block<T>, catchDefault: CatchDefault<T> = ::rethrowingCatch, finally: Finally<T>, vararg catches: Catch<*, T>): T =
//    try {
//        block.invoke()
//    } catch (e: Exception) {
//        for (catch in catches) {
//            catch::class::allSupertypes
//        }
//    } finally {
//        finally.invoke()
//    }

interface ResultAccessor<T> { val returned: T }

@JvmInline
value class Result<T>(override val returned: T) : ResultAccessor<T>

fun rethrowingCatch(e: Exception): Nothing = throw e

typealias Block<T> = () -> T
typealias Catch<E, T> = (e: E) -> T
typealias CatchDefault<T> = (e: Exception) -> T
typealias Finally<T> = () -> T
typealias WhenCatches<T> = (e: Exception) -> ResultAccessor<T>?

typealias BlockT<O, T> = O.() -> T
typealias CatchT<O, E, T> = O.(e: E) -> T
typealias CatchDefaultT<O, T> = O.(e: Exception) -> T
typealias FinallyT<O, T> = O.() -> T
typealias WhenCatchesT<O, T> = O.(e: Exception) -> ResultAccessor<T>?



inline fun <T> SmartTry.tryCatchingVararg(
    crossinline block: Block<T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline whenCatches: WhenCatches<T>
) =
    try {
        block.invoke()
    } catch (e: Exception) {
        whenCatches.invoke(e)?.returned ?: catchDefault.invoke(e)
    }

inline fun <T> SmartTry.tryCatchingVarargFinally(
    crossinline block: Block<T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline finally: Finally<T>,
    crossinline whenCatches: WhenCatches<T>
) =
    try {
        block.invoke()
    } catch (e: Exception) {
        whenCatches.invoke(e)?.returned ?: catchDefault.invoke(e)
    } finally {
        finally.invoke()
    }


inline fun <O, T> SmartTry.tryCatchingVarargTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
    crossinline whenCatches: WhenCatchesT<O, T>
) =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        whenCatches.invoke(transited, e)?.returned ?: catchDefault.invoke(transited, e)
    }

inline fun <O, T> SmartTry.tryCatchingVarargFinally(
    transited: O,
    crossinline block: BlockT<O,T>,
    crossinline catchDefault: CatchDefaultT<O,T> = {with(it,::rethrowingCatch)},
    crossinline finally: FinallyT<O,T>,
    crossinline whenCatches: WhenCatchesT<O,T>
) =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        whenCatches.invoke(transited,e)?.returned ?: catchDefault.invoke(transited,e)
    } finally {
        finally.invoke(transited)
    }


@Throws(Exception::class)
inline fun <T> SmartTry.tryCatchingDefault(
    crossinline block: Block<T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        catchDefault.invoke(e)
    }

@Throws(Exception::class)
inline fun <O, T> SmartTry.tryCatchingDefaultTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        catchDefault.invoke(transited, e)
    }

@Throws(Exception::class)
inline fun <T> SmartTry.tryCatchingDefaultFinally(
    crossinline block: Block<T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline finally: Finally<T>
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        catchDefault.invoke(e)
    } finally {
        finally.invoke()
    }

@Throws(Exception::class)
inline fun <O, T> SmartTry.tryCatchingDefaultFinallyTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
    crossinline finally: FinallyT<O, T>
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        catchDefault.invoke(transited, e)
    } finally {
        finally.invoke(transited)
    }

inline fun <reified E : Exception, T> SmartTry.tryCatching(
    crossinline block: Block<T>,
    crossinline catch: Catch<E, T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
): T = try {
    block.invoke()
} catch (e: Exception) {
    when {
        E::class.isSuperclassOf(e::class) -> catch.invoke(e as E)
        else -> catchDefault.invoke(e)
    }
}


inline fun <reified E : Exception, O, T> SmartTry.tryCatchingTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch: CatchT<O, E, T>,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) }
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E::class.isSuperclassOf(e::class) -> catch.invoke(transited, e as E)
            else -> catchDefault.invoke(transited, e)
        }
    }


inline fun <reified E : Exception, T> SmartTry.tryCatchingFinally(
    crossinline block: Block<T>,
    crossinline catch: Catch<E, T>,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline finally: Finally<T>
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        when {
            E::class.isSuperclassOf(e::class) -> catch.invoke(e as E)
            else -> catchDefault.invoke(e)
        }
    } finally {
        finally.invoke()
    }

inline fun <reified E : Exception, O, T> SmartTry.tryCatchingFinallyTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch: CatchT<O, E, T>,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
    crossinline finally: FinallyT<O, T>
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E::class.isSuperclassOf(e::class) -> catch.invoke(transited, e as E)
            else -> catchDefault.invoke(transited, e)
        }
    } finally {
        finally.invoke(transited)
    }


inline fun <reified E1 : Exception, reified E2 : Exception, T> SmartTry.tryCatching2(
    crossinline block: Block<T>,
    crossinline catch1: (e: E1) -> T,
    crossinline catch2: (e: E2) -> T,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(e as E2)
            else -> catchDefault(e)
        }
    }

inline fun <reified E1 : Exception, reified E2 : Exception, O, T> SmartTry.tryCatching2Transitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch1: O.(e: E1) -> T,
    crossinline catch2: O.(e: E2) -> T,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) }
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(transited, e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(transited, e as E2)
            else -> catchDefault(transited, e)
        }
    }

inline fun <reified E1 : Exception, reified E2 : Exception, T> SmartTry.tryCatching2Finally(
    crossinline block: Block<T>,
    crossinline catch1: (e: E1) -> T,
    crossinline catch2: (e: E2) -> T,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline finally: Finally<T>
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(e as E2)
            else -> catchDefault(e)
        }
    } finally {
        finally.invoke()
    }

inline fun <reified E1 : Exception, reified E2 : Exception, O, T> SmartTry.tryCatching2FinallyTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch1: O.(e: E1) -> T,
    crossinline catch2: O.(e: E2) -> T,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
    crossinline finally: FinallyT<O, T>
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(transited, e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(transited, e as E2)
            else -> catchDefault(transited, e)
        }
    } finally {
        finally.invoke(transited)
    }


inline fun <reified E1 : Exception, reified E2 : Exception, reified E3 : Exception, T> SmartTry.tryCatching3(
    crossinline block: Block<T>,
    crossinline catch1: (e: E1) -> T,
    crossinline catch2: (e: E2) -> T,
    crossinline catch3: (e: E3) -> T,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(e as E2)
            E3::class.isSuperclassOf(e::class) -> catch3(e as E3)
            else -> catchDefault(e)
        }
    }

inline fun <reified E1 : Exception, reified E2 : Exception, reified E3 : Exception, O, T> SmartTry.tryCatching3Transitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch1: O.(e: E1) -> T,
    crossinline catch2: O.(e: E2) -> T,
    crossinline catch3: O.(e: E3) -> T,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) }
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(transited, e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(transited, e as E2)
            E3::class.isSuperclassOf(e::class) -> catch3(transited, e as E3)
            else -> catchDefault(transited, e)
        }
    }

inline fun <reified E1 : Exception, reified E2 : Exception, reified E3 : Exception, O, T> SmartTry.tryCatching3Finally(
    crossinline block: Block<T>,
    crossinline catch1: (e: E1) -> T,
    crossinline catch2: (e: E2) -> T,
    crossinline catch3: (e: E3) -> T,
    crossinline catchDefault: CatchDefault<T> = ::rethrowingCatch,
    crossinline finally: Finally<T>
): T =
    try {
        block.invoke()
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(e as E2)
            E3::class.isSuperclassOf(e::class) -> catch3(e as E3)
            else -> catchDefault(e)
        }
    } finally {
        finally.invoke()
    }

inline fun <reified E1 : Exception, reified E2 : Exception, reified E3 : Exception, O, T> SmartTry.tryCatching3FinallyTransitive(
    transited: O,
    crossinline block: BlockT<O, T>,
    crossinline catch1: O.(e: E1) -> T,
    crossinline catch2: O.(e: E2) -> T,
    crossinline catch3: O.(e: E3) -> T,
    crossinline catchDefault: CatchDefaultT<O, T> = { with(it, ::rethrowingCatch) },
    crossinline finally: FinallyT<O, T>
): T =
    try {
        block.invoke(transited)
    } catch (e: Exception) {
        when {
            E1::class.isSuperclassOf(e::class) -> catch1(transited, e as E1)
            E2::class.isSuperclassOf(e::class) -> catch2(transited, e as E2)
            E3::class.isSuperclassOf(e::class) -> catch3(transited, e as E3)
            else -> catchDefault(transited, e)
        }
    } finally {
        finally.invoke(transited)
    }

