package ru.itis.addressdatabaseupdater.services

import jakarta.transaction.Transactional
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.Code2Char

import ru.itis.addressdatabaseupdater.errors.LanguagesRepositoryException
import ru.itis.addressdatabaseupdater.models.*
import ru.itis.addressdatabaseupdater.repositories.SubdivisionsRepository
import java.io.Serial
import kotlin.jvm.optionals.getOrNull

class SubdivisionsRepositoryException(e: Throwable) : Exception(e) {
    companion object {
        @Serial
        private const val serialVersionUID: Long = 8137916698787520795L
    }
}

interface SubdivisionsService {
    @Throws(SubdivisionsRepositoryException::class)
    fun save(subdivisionCode: Iso3166SecondLevel<*, *>): Iso3166SecondLevel<*, *>

    @Throws(SubdivisionsRepositoryException::class)
    fun findAll(): List<Iso3166SecondLevel<*, *>>

    @Throws(SubdivisionsRepositoryException::class)
    fun findById(id: Long): Iso3166SecondLevel<*, *>?

    @Throws(SubdivisionsRepositoryException::class)
    fun deleteById(id: Long)

    @Throws(SubdivisionsRepositoryException::class)
    fun saveAll(subdivisionCodes: Iterable<Iso3166SecondLevel<*, *>>): Iterable<Iso3166SecondLevel<*, *>>

    @Throws(SubdivisionsRepositoryException::class)
    fun findIso3166SecondLevelByCode3Char(stringToCode3Char: Code3Char): Iso3166SecondLevel<*, *>?

    @Throws(SubdivisionsRepositoryException::class)
    fun findIso3166SecondLevelsByDescendantsIsNull(): List<Iso3166SecondLevel<*, HasNoChild>>

    @Throws(SubdivisionsRepositoryException::class)
    fun findIso3166SecondLevelsByParentIsNull(): List<Iso3166SecondLevel<HasNotParent, *>>

    @Throws(SubdivisionsRepositoryException::class)
    fun findIso3166SecondLevelsByDescendantsIsNotNull(): List<Iso3166SecondLevel<*, HasChild>>

    @Throws(SubdivisionsRepositoryException::class)
    fun findIso3166SecondLevelsByParentIsNotNull(): List<Iso3166SecondLevel<HasParent, *>>


    //    Lonely
    fun findIso3166SecondLevelsLonely(): List<Iso3166SecondLevelLonely>

    //    OnlyChild
    fun findIso3166SecondLevelsOnlyChild(): List<Iso3166SecondLevelOnlyChild>

    //    OnlyParent
    fun findIso3166SecondLevelsOnlyParent(): List<Iso3166SecondLevelOnlyParent>

    //    Surrounded
    fun findIso3166SecondLevelsParentAndChild(): List<Iso3166SecondLevelParentNChild>


    fun findIso3166SecondLevelByCountryCodeAndSubdivisionCode(
        parentCode: Code2Char,
        childCode: Code3Char
    ): Iso3166SecondLevel<*, *>?
}

@Transactional
@Service
class SubdivisionsServiceImpl(val repository: SubdivisionsRepository) : SubdivisionsService {
    override fun save(subdivisionCode: Iso3166SecondLevel<*, *>): Iso3166SecondLevel<*, *> =
        runCatching { repository.save(subdivisionCode) }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun findAll(): List<Iso3166SecondLevel<*, *>> = runCatching<List<Iso3166SecondLevel<*, *>>> {
        repository.findAll().toList()
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun findById(id: Long): Iso3166SecondLevel<*, *>? = runCatching<Iso3166SecondLevel<*, *>?> {
        repository.findById(id).getOrNull()
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun deleteById(id: Long) =
        kotlin.runCatching { repository.deleteById(id) }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun saveAll(subdivisionCodes: Iterable<Iso3166SecondLevel<*, *>>): Iterable<Iso3166SecondLevel<*, *>> =
        runCatching { repository.saveAll(subdivisionCodes) }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun findIso3166SecondLevelByCode3Char(stringToCode3Char: Code3Char): Iso3166SecondLevel<*, *>? =
        runCatching<Iso3166SecondLevel<*, *>?> {
            repository.findIso3166SecondLevelByCode3char(stringToCode3Char.toAlias())
        }.getOrElse { throw SubdivisionsRepositoryException(it) }


    @Throws(SubdivisionsRepositoryException::class)
    override fun findIso3166SecondLevelsByDescendantsIsNull(): List<Iso3166SecondLevel<*, HasNoChild>> =
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNull() as List<Iso3166SecondLevel<*, HasNoChild>>

    @Throws(SubdivisionsRepositoryException::class)
    override fun findIso3166SecondLevelsByParentIsNull(): List<Iso3166SecondLevel<HasNotParent, *>> = kotlin.runCatching<List<Iso3166SecondLevel<HasNotParent, *>>> {
        repository.findIso3166SecondLevelsByParentProtectedIsNull() as List<Iso3166SecondLevel<HasNotParent, *>>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    @Throws(SubdivisionsRepositoryException::class)
    override fun findIso3166SecondLevelsByDescendantsIsNotNull(): List<Iso3166SecondLevel<*, HasChild>> = kotlin.runCatching<List<Iso3166SecondLevel<*, HasChild>>> {
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNotNull() as List<Iso3166SecondLevel<*, HasChild>>

    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    @Throws(SubdivisionsRepositoryException::class)
    override fun findIso3166SecondLevelsByParentIsNotNull(): List<Iso3166SecondLevel<HasParent, *>> = kotlin.runCatching<List<Iso3166SecondLevel<HasParent, *>>> {
        repository.findIso3166SecondLevelsByParentProtectedIsNotNull() as List<Iso3166SecondLevel<HasParent, *>>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }


    //    Lonely
    override fun findIso3166SecondLevelsLonely(): List<Iso3166SecondLevelLonely>  = kotlin.runCatching<List<Iso3166SecondLevelLonely>> {
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNullAndParentProtectedIsNull() as List<Iso3166SecondLevelLonely>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    //    OnlyChild
    override fun findIso3166SecondLevelsOnlyChild(): List<Iso3166SecondLevelOnlyChild>  = kotlin.runCatching<List<Iso3166SecondLevelOnlyChild>> {
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNullAndParentProtectedIsNotNull() as List<Iso3166SecondLevelOnlyChild>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }


    //    OnlyParent
    override fun findIso3166SecondLevelsOnlyParent(): List<Iso3166SecondLevelOnlyParent> = kotlin.runCatching<List<Iso3166SecondLevelOnlyParent>> {
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNotNullAndParentProtectedIsNull() as List<Iso3166SecondLevelOnlyParent>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    //    Surrounded
    override fun findIso3166SecondLevelsParentAndChild(): List<Iso3166SecondLevelParentNChild>  = kotlin.runCatching {
        repository.findIso3166SecondLevelsByDescendantsProtectedIsNotNullAndParentProtectedIsNotNull() as List<Iso3166SecondLevelParentNChild>
    }.getOrElse { throw SubdivisionsRepositoryException(it) }

    override fun findIso3166SecondLevelByCountryCodeAndSubdivisionCode(parentCode: Code2Char, childCode: Code3Char): Iso3166SecondLevel<*,*>? = kotlin.runCatching {
        repository.findIso3166SecondLevelByCode3charAndLinkedBy_Code2char(childCode.toAlias(), parentCode.toAlias())
    }.getOrElse { throw SubdivisionsRepositoryException(it) }
}

