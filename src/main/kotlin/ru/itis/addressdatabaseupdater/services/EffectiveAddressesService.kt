package ru.itis.addressdatabaseupdater.services

import jakarta.transaction.Transactional
import org.springframework.stereotype.Service
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.errors.LanguagesRepositoryException
import ru.itis.addressdatabaseupdater.models.Iso639FirstLevel
import ru.itis.addressdatabaseupdater.models.EffectiveAddressInfo
import ru.itis.addressdatabaseupdater.dto.FilePath
import ru.itis.addressdatabaseupdater.repositories.EffectiveAddressesRepository
import kotlin.jvm.optionals.getOrNull


class EffectiveAddressesServiceException(e: Throwable) : RuntimeException(e)

interface EffectiveAddressesService {
    @Throws(EffectiveAddressesServiceException::class)
    fun save(langCode: EffectiveAddressInfo): EffectiveAddressInfo

    @Throws(EffectiveAddressesServiceException::class)
    fun findAll(): List<EffectiveAddressInfo>

    @Throws(EffectiveAddressesServiceException::class)
    fun findById(id: Long): EffectiveAddressInfo?

    @Throws(EffectiveAddressesServiceException::class)
    fun deleteById(id: Long)

    @Throws(EffectiveAddressesServiceException::class)
    fun saveAll(langCodes: Iterable<EffectiveAddressInfo>): Iterable<EffectiveAddressInfo>
    @Throws(EffectiveAddressesServiceException::class)
    fun findEffectiveAddressInfoByFilePath(filePath: FilePath): EffectiveAddressInfo?
}

@Transactional
@Service
class EffectiveAddressesServiceImpl(
    protected val effectiveAddressesRepository: EffectiveAddressesRepository
) : EffectiveAddressesService {
    override fun save(langCode: EffectiveAddressInfo): EffectiveAddressInfo = runCatching { effectiveAddressesRepository.save(langCode) }.getOrElse { throw EffectiveAddressesServiceException(it) }

    override fun findAll(): List<EffectiveAddressInfo> = runCatching { effectiveAddressesRepository.findAll() }.getOrElse { throw EffectiveAddressesServiceException(it) }

    override fun findById(id: Long): EffectiveAddressInfo? = runCatching { effectiveAddressesRepository.findById(id).getOrNull() }.getOrElse { throw EffectiveAddressesServiceException(it) }

    override fun deleteById(id: Long) = runCatching { effectiveAddressesRepository.deleteById(id) }.getOrElse { throw EffectiveAddressesServiceException(it) }

    override fun saveAll(langCodes: Iterable<EffectiveAddressInfo>): Iterable<EffectiveAddressInfo> = runCatching { effectiveAddressesRepository.saveAll(langCodes) }.getOrElse { throw EffectiveAddressesServiceException(it) }

    override fun findEffectiveAddressInfoByFilePath(filePath: FilePath): EffectiveAddressInfo? = runCatching { effectiveAddressesRepository.findEffectiveAddressInfoByFilePath(filePath) }.getOrElse { throw EffectiveAddressesServiceException(it) }
}