package ru.itis.addressdatabaseupdater.services

import jakarta.transaction.Transactional
import org.springframework.stereotype.Service
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.errors.IllegalArgumentHandler
import ru.itis.addressdatabaseupdater.errors.LanguagesRepositoryException
import ru.itis.addressdatabaseupdater.errors.OptimisticLockingFailureHandler
import ru.itis.addressdatabaseupdater.errors.SpringDataJdbcHandler
import ru.itis.addressdatabaseupdater.models.Iso639FirstLevel
import ru.itis.addressdatabaseupdater.models.toAlias
import ru.itis.addressdatabaseupdater.repositories.LanguagesRepository
import ru.itis.addressdatabaseupdater.utils.commons.*
import java.io.InputStream
import kotlin.io.path.Path
import kotlin.io.path.inputStream
import kotlin.jvm.optionals.getOrNull


interface LanguagesService {
    @Throws(LanguagesRepositoryException::class)
    fun save(langCode: Iso639FirstLevel): Iso639FirstLevel

    @Throws(LanguagesRepositoryException::class)
    fun findAll(): List<Iso639FirstLevel>

    @Throws(LanguagesRepositoryException::class)
    fun findById(id: Long): Iso639FirstLevel?

    @Throws(LanguagesRepositoryException::class)
    fun deleteById(id: Long)

    @Throws(LanguagesRepositoryException::class)
    fun saveAll(langCodes: Iterable<Iso639FirstLevel>): Iterable<Iso639FirstLevel>
    @Throws(LanguagesRepositoryException::class)
    fun findIso639FirstLevelByCode2Char(code: Code2Char): Iso639FirstLevel?
}

@Transactional
@Service
class LanguagesServiceImpl(val repository: LanguagesRepository) : LanguagesService {
    @Throws(LanguagesRepositoryException::class)
    override fun save(langCode: Iso639FirstLevel): Iso639FirstLevel = LanguagesRepositoryException.with {
        smartTry {
            tryCatching3(
                { repository.save(langCode) },
                throws(
                    "IllegalArgumentException in LanguagesRepository while saving language code: $langCode",
                    IllegalArgumentHandler::handle
                ),
                throws(
                    "OptimisticLockingFailureException in LanguagesRepository while saving language code: $langCode",
                    OptimisticLockingFailureHandler::handle
                ),
                throwsAncestor(
                    Scope(),
                    "Hibernate SqlException in LanguagesRepository while saving language code: $langCode",
                    SpringDataJdbcHandler::handle
                )
            )
        }
    }

    @Throws(LanguagesRepositoryException::class)
    override fun findById(id: Long): Iso639FirstLevel? = LanguagesRepositoryException.with {
        smartTry {
            tryCatchingDefaultFinallyTransitive(object {
                var inputStream: InputStream? = null
            }, {
                inputStream = Path("").inputStream()
            },
                { with(it, ::rethrowingCatch) }
            ) { inputStream?.close() }
        }

        smartTry {
            tryCatching2(
                { repository.findById(id).getOrNull() },
                throws(
                    "IllegalArgumentException in LanguagesRepository while finding language code by id: $id",
                    IllegalArgumentHandler::handle
                ),
                throws(
                    "Hibernate SqlException in LanguagesRepository while finding language code: $id",
                    SpringDataJdbcHandler::handle
                )
            )
        }
    }


    @Throws(LanguagesRepositoryException::class)
    override fun findAll(): List<Iso639FirstLevel> = repository.findAll().toList()


    @Throws(LanguagesRepositoryException::class)
    override fun deleteById(id: Long) = LanguagesRepositoryException.with {
        smartTry {
            tryCatching2(
                { repository.deleteById(id) },
                throws(
                    "IllegalArgumentException in LanguagesRepository while deleting language code by id: $id",
                    IllegalArgumentHandler::handle
                ),
                throws(
                    "Hibernate SqlException in LanguagesRepository while deleting language code: $id",
                    SpringDataJdbcHandler::handle
                )
            )
        }
    }

    @Throws(LanguagesRepositoryException::class)
    override fun saveAll(langCodes: Iterable<Iso639FirstLevel>): Iterable<Iso639FirstLevel> =
        LanguagesRepositoryException.with {
            smartTry {
                tryCatching3(
                    { repository.saveAll(langCodes) },
                    throws(
                        "IllegalArgumentException in LanguagesRepository while saving language codes: $langCodes",
                        IllegalArgumentHandler::handle
                    ),
                    throws(
                        "OptimisticLockingFailureException in LanguagesRepository while saving language codes: $langCodes",
                        OptimisticLockingFailureHandler::handle
                    ),
                    throws(
                        "Hibernate SqlException in LanguagesRepository while saving language codes: $langCodes",
                        SpringDataJdbcHandler::handle
                    )
                )
            }
        }


    @Throws(LanguagesRepositoryException::class)
    override fun findIso639FirstLevelByCode2Char(code: Code2Char): Iso639FirstLevel? =
        LanguagesRepositoryException.with {
            smartTry {
                tryCatching3(
                    { repository.findIso639FirstLevelByCode2char(code.toAlias()) },
                    throws(
                        "IllegalArgumentException in LanguagesRepository while find Iso639FirstLevels By Code2Char: $code",
                        IllegalArgumentHandler::handle
                    ),
                    throws(
                        "OptimisticLockingFailureException in LanguagesRepository while findIso639FirstLevels By Code2Char: $code",
                        OptimisticLockingFailureHandler::handle
                    ),
                    throws(
                        "Hibernate SqlException in LanguagesRepository while find Iso639FirstLevels By Code2Char: $code",
                        SpringDataJdbcHandler::handle
                    )
                )
            }
        }

}
