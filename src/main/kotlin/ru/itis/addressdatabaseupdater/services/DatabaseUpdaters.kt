package ru.itis.addressdatabaseupdater.services

import org.springframework.stereotype.Component
import ru.itis.addressdatabaseupdater.configs.properties.DownloadManifest
import ru.itis.addressdatabaseupdater.parsers.OpenAddressesConverter

@Component

class DatabaseUpdaters(val converter: OpenAddressesConverter, val manifest: DownloadManifest) {
    //TODO
//    @PostConstruct
//    fun init() {
////        WebService.setUserName("demo")
//
//        val path = "/psqldata/temp/"
//
//        val configured = ArchivedFileStreamer.instance(descriptor = Zip4JProvider.getDescriptor(),
//            fileStreamState = FileStreamState(
//                iterateState = IterateState(
//                    filesFilter =  {it.holden.fileName.matches(Regex("""(.*(-buildings-|-parcels-).*)|(.*meta)"""))}
//
//                    ,
//                    archiveInformation = Zip4JInformation(UnzipParameters())
//                ),
//                streamsNewURIProvider = { i, fd -> fd.holden.fileName + ".extracted.txt" }
//            ))
//
//        val data = runBlocking {
//            configured.getFileStreams(Path("""/psqldata/databases/downloads/openaddresses.io/collection-global.zip"""))
//            {
//                for ((pat, address, err) in convertFilesToAddresses(converter, it)){
//                    println(address?.house ?: err!!)
//                }
//            }
//
//        }
//    }
}