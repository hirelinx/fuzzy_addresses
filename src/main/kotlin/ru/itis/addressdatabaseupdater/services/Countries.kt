package ru.itis.addressdatabaseupdater.services

import jakarta.transaction.Transactional
import org.springframework.stereotype.Service
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.models.*
import ru.itis.addressdatabaseupdater.repositories.CountriesRepository
import ru.itis.addressdatabaseupdater.repositories.LanguagesRepository
import java.util.*
import kotlin.jvm.optionals.getOrNull

class CountiesRepositoryException(e: Throwable) : RuntimeException(e)


interface CountriesService {
    @Throws(CountiesRepositoryException::class)
    fun save(langCode: Iso3166FirstLevel): Iso3166FirstLevel

    @Throws(CountiesRepositoryException::class)

    fun findAll(): List<Iso3166FirstLevel>

    @Throws(CountiesRepositoryException::class)

    fun findById(id: Long): Iso3166FirstLevel?

    @Throws(CountiesRepositoryException::class)

    fun deleteById(id: Long)

    @Throws(CountiesRepositoryException::class)
    fun saveAll(langCodes: Iterable<Iso3166FirstLevel>): Iterable<Iso3166FirstLevel>

    @Throws(CountiesRepositoryException::class)
    fun findIso3166FirstLevelByCode2Char(code2Char: Code2Char): Iso3166FirstLevel?
}

@Transactional
@Service
class CountriesServiceImpl(val repository: CountriesRepository, val languagesRepository: LanguagesRepository) :
    CountriesService {
    override fun save(langCode: Iso3166FirstLevel): Iso3166FirstLevel =
        runCatching { repository.save(langCode) }.getOrElse { throw CountiesRepositoryException(it) }

    override fun findAll(): List<Iso3166FirstLevel> =
        runCatching { repository.findAll().toList() }.getOrElse { throw CountiesRepositoryException(it) }

    override fun findById(id: Long): Iso3166FirstLevel? =
        runCatching { repository.findById(id).getOrNull() }.getOrElse { throw CountiesRepositoryException(it) }

    override fun deleteById(id: Long) =
        runCatching { repository.deleteById(id) }.getOrElse { throw CountiesRepositoryException(it) }

    override fun saveAll(langCodes: Iterable<Iso3166FirstLevel>): Iterable<Iso3166FirstLevel> =
        runCatching { repository.saveAll(langCodes) }.getOrElse { throw CountiesRepositoryException(it) }

    override fun findIso3166FirstLevelByCode2Char(code2Char: Code2Char): Iso3166FirstLevel? =
        runCatching { repository.findIso3166FirstLevelByCode2char(code2Char.toAlias()) }.getOrElse { throw CountiesRepositoryException(it) }
}
