package ru.itis.addressdatabaseupdater.services

import com.mapzen.jpostal.AddressParser
import com.mapzen.jpostal.AddressExpander
import com.mapzen.jpostal.ParsedComponent
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import ru.itis.addressdatabaseupdater.models.*

//interface AddressTransformerService {
//    fun parse(addressS: String): Pair<Address, Array<ParsedComponent>>
//    fun expand(addressS: String): String
//}
//
//@Service
//class AddressTransformerServiceImpl(@Qualifier("labelsEnumHelper") private val labelsEnumHelper: EnumHelper<LabelsEnum>, private val parser: AddressParser, private val expander: AddressExpander): AddressTransformerService {
//
//    override fun parse(addressS: String): Pair<Address, Array<ParsedComponent>> {
//        val parsed = parser.parseAddress(addressS)
//        return Pair(Address(labelsEnumHelper, parsed), parsed)
//    }
//
//    override fun expand(addressS: String): String {
//        val expanded = expander.expandAddress(addressS)
//        return expanded[0]?: ""
//    }


//    fun findMessages(): List<Address> = db.query("select * from messages") { response, _ ->
//        Address(response.getString("id"), response.getString("text"))
//    }
//
//    fun save(address: Address) {
//        val id: String = address.id ?: UUID.randomUUID().toString()
//
//        db.update(
//                "insert into messages values ( ?, ? )",
//            id, address.text
//        )
//    }
//}