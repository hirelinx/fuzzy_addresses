package ru.itis.addressdatabaseupdater.models

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

data class GeoJson(val type: GeoJsonTypes, val properties: Map<Properties, String>, val geometry: GeometryType) {
    enum class GeoJsonTypes {
        Feature
    }

    enum class Properties {
        hash,
        number,
        street,
        unit,
        city,
        district,
        region,
        postcode,
        id,
    }

    @JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
    )
    @JsonSubTypes(
        JsonSubTypes.Type(value = Point::class, name = "Point")
    )
    abstract class GeometryType

    data class Point(val coordinates: List<Double>) : GeometryType()
}