package ru.itis.addressdatabaseupdater.models

import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.IsoToponym
import ru.itis.addressdatabaseupdater.dto.Language
import ru.itis.addressdatabaseupdater.dto.FilePath



internal typealias Code2CharAlias = String
internal typealias Code3CharAlias = String
internal typealias IsoToponymAlias = String
internal typealias LanguageAlias = String
internal typealias FilePathAlias = String

internal inline fun Code2Char.toAlias() = this.value
internal inline fun Code3Char.toAlias() = this.value
internal inline fun IsoToponym.toAlias() = this.name
internal inline fun Language.toAlias() = this.name
internal fun FilePath.toAlias(): FilePathAlias = this.name


inline fun <reified S: Any> String.fromAlias(): S = when (S::class) {
    Code2Char::class -> Code2Char(this) as S
    Code3Char::class -> Code3Char(this) as S
    IsoToponym::class -> IsoToponym(this) as S
    Language::class -> Language(this) as S
    FilePath::class -> FilePath(this) as S
    else -> throw IllegalArgumentException("bad class: ${S::class.qualifiedName}")
}