package ru.itis.addressdatabaseupdater.models

import kotlin.enums.EnumEntries
import kotlin.enums.enumEntries

@ExperimentalStdlibApi
inline fun <reified T : Enum<T>> enumLength(): Int = enumEntries<T>().size

@ExperimentalStdlibApi
inline fun <reified T : Enum<T>> printAllValues(): String = enumEntries<T>().joinToString { it.name }

interface EnumHelper<E : Enum<E>> {
    fun getOrdinal(name: String): Int?
    fun getLabel(ordinal: Int): String?
}

//interface WordWiseCaseInsensitiveEnumeration {
//    val parts: Array<String>
//}
//
//enum class ExpandMode() {
//    flatcase,
//    UPPERCASE,
//    camelCase,
//    PascalCase,
//    snake_case;
//}
//
//fun getWordWiseCase(str: String) : ExpandMode {
//
//}
//
//interface EnumExpanderHelper<E : Enum<E>> : EnumHelper<E>{
//
//}

@ExperimentalStdlibApi
open class EnumHelperImpl<E : Enum<E>>(entries: EnumEntries<E>) : EnumHelper<E> {

    protected val stringToOrdinal: MutableMap<String, Int> = mutableMapOf()
    protected val ordinalToString: MutableMap<Int, String> = mutableMapOf()

    init {
        entries.forEach { entry ->
            stringToOrdinal[entry.name] = entry.ordinal
            ordinalToString[entry.ordinal] = entry.name

        }
    }

    override fun getOrdinal(name: String): Int? = stringToOrdinal[name]
    override fun getLabel(ordinal: Int): String? = ordinalToString[ordinal]

}



@ExperimentalStdlibApi
inline fun <reified E : Enum<E>> buildEnumHelperImpl(): EnumHelperImpl<E> = EnumHelperImpl(enumEntries<E>())