package ru.itis.addressdatabaseupdater.models

import jakarta.persistence.*
import ru.itis.addressdatabaseupdater.utils.commons.Stringer
import au.com.console.kassava.kotlinToString
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.IsoToponym
import ru.itis.addressdatabaseupdater.dto.Language
import java.io.Serializable
import ru.itis.addressdatabaseupdater.utils.commons.Equaler
import ru.itis.addressdatabaseupdater.utils.commons.HashCoder
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonView
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import ru.itis.addressdatabaseupdater.views.IsoViews


fun Iso3166SecondLevel<*, *>.fullCode() = "%S-%S".format(this.linkedBy.code2char, this.code3char)


interface Iso {
    val coded: String
}

interface IsoFirstLevel : Iso {
    val code2char: Code2CharAlias
    val code3char: Code3CharAlias
}

interface IsoSecondLevel : Iso {
    val code3char: Code3CharAlias
}


@MappedSuperclass
abstract class IsoFirstLevelEntity(
    @Column(
        length = 2,
        nullable = false,
        unique = true,
        name = "code2char"
    )
    override var code2char: Code2CharAlias,
    @Column(
        length = 3,
        nullable = false,
        unique = true,
        name = "code3char"
    )
    override var code3char: Code3CharAlias,
) : IsoFirstLevel, Stringer {
    @Id
    @GeneratedValue
    @Column(name = "iso1lvl_id")
    open var id: Long? = null
}

@MappedSuperclass
abstract class IsoSecondLevelEntity(
    @Column(
        length = 3,
        nullable = false,
        name = "code3char"
    )
    override var code3char: Code3CharAlias,
) : IsoSecondLevel, Stringer {
    @Id
    @GeneratedValue
    @Column(name = "iso2lvl_id")
    open var id: Long? = null
}

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@Entity
@Table(name = "ISO_639")
class Iso639FirstLevel(
    code2char: Code2CharAlias,
    code3char: Code3CharAlias,
    var language: LanguageAlias,
    @JsonView(IsoViews.FlatMapLanguage::class)
    @ManyToMany(mappedBy = "languages")
    open var countries: MutableSet<Iso3166FirstLevel>,
    internal: Boolean = true
) : IsoFirstLevelEntity(code2char, code3char) {

    constructor(
        code2char: Code2Char,
        code3char: Code3Char,
        language: Language,
        countries: MutableSet<Iso3166FirstLevel>
    ) : this(code2char.toAlias(), code3char.toAlias(), language.toAlias(), countries)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso639FirstLevel::id,
            Iso639FirstLevel::code2char,
            Iso639FirstLevel::code3char,
            Iso639FirstLevel::language
        )
    }

    @get:Transient
    override val coded: String by ::language
    override fun toString(): String = kotlinToString(properties = properties)
}

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@Entity
@Table(name = "ISO_3166_1")
class Iso3166FirstLevel(
    code2char: Code2CharAlias,
    code3char: Code3CharAlias,
    var country: IsoToponymAlias,
    @JsonView(IsoViews.FlatMapCountry::class)
    @OneToMany(mappedBy = "linkedBy", cascade = [CascadeType.ALL])
    open var subdivisions: MutableSet<Iso3166SecondLevel<*, *>>,
    @JsonView(IsoViews.FlatMapCountry::class)
    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "CountryToLanguageAlias",
        joinColumns = [JoinColumn(name = "country_id")],
        inverseJoinColumns = [JoinColumn(name = "language_id")]
    )
    open var languages: MutableSet<Iso639FirstLevel>,
    internal: Boolean = true
) : IsoFirstLevelEntity(code2char, code3char) {

    constructor(
        code2char: Code2Char,
        code3char: Code3Char,
        country: IsoToponym,
        subdivisions: MutableSet<Iso3166SecondLevel<*, *>>,
        languages: MutableSet<Iso639FirstLevel>
    ) : this(code2char.toAlias(), code3char.toAlias(), country.toAlias(), subdivisions, languages)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso3166FirstLevel::id,
            Iso3166FirstLevel::code2char,
            Iso3166FirstLevel::code3char,
            Iso3166FirstLevel::country,
        )
    }

    @Transient
    override var coded: IsoToponymAlias = country
    override fun toString(): String = kotlinToString(properties = properties)

}


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@Entity
@Table(
    name = "ISO_3166_2",
    uniqueConstraints = [UniqueConstraint(
        name = "iso1lvl_id_and_code3char_unique",
        columnNames = ["iso1lvl_id", "code3char"]
    )]
)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "iso3166_2_type",
    discriminatorType = DiscriminatorType.CHAR
)
@DiscriminatorValue("*")
abstract class Iso3166SecondLevel<P : ParentRelation, D : ChildRelation> protected constructor(
    code3char: Code3CharAlias,
    open var subdivision: IsoToponymAlias,
    @JsonView(IsoViews.FlatMapSubdivisionNonRecursiveInternally::class)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "iso1lvl_id", nullable = false)
    open var linkedBy: Iso3166FirstLevel,

    @JsonView(IsoViews.FlatMapSubdivisionNonRecursiveInternally::class)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    open var parentProtected: Iso3166SecondLevel<*, HasChild>?,

    @JsonView(IsoViews.FlatMapSubdivisionRecursiveInternally::class)
    @OneToMany(mappedBy = "parentProtected", cascade = [CascadeType.ALL] , fetch = FetchType.LAZY)
    open var descendantsProtected: MutableSet<Iso3166SecondLevel<HasParent, *>>?,

    internal: Boolean = true
) : IsoSecondLevelEntity(code3char), IsoSecondLevel {
    constructor(
        code3char: Code3Char,
        subdivision: IsoToponym,
        linkedBy: Iso3166FirstLevel,
        parent: Iso3166SecondLevel<*, HasChild>,
        descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>,
    ) : this(code3char.toAlias(), subdivision.toAlias(), linkedBy, parent, descendants)

    @Transient
    override var coded: IsoToponymAlias = subdivision

    @get:Transient
    val fullCode
        get() = fullCode()
}

sealed interface ChildRelation

sealed interface HasNoChild : ChildRelation

sealed interface HasChild : ChildRelation {
    var descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>
}

sealed interface ParentRelation


sealed interface HasNotParent : ParentRelation

sealed interface HasParent : ParentRelation {
    var parent: Iso3166SecondLevel<*, HasChild>
}


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@DiscriminatorValue("P")
@Entity(name = "iso3166_2_P")
class Iso3166SecondLevelOnlyParent protected constructor(
    code3char: Code3CharAlias,
    subdivision: IsoToponymAlias,
    linkedBy: Iso3166FirstLevel,
    descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>,
    internal: Boolean = true
) : Iso3166SecondLevel<HasNotParent, HasChild>(code3char, subdivision, linkedBy, null, descendants), HasNotParent,
    HasChild {
    override var descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>
        get() = super.descendantsProtected!!
        set(e) {
            super.descendantsProtected = e
        }

    constructor(
        code3char: Code3Char,
        subdivision: IsoToponym,
        linkedBy: Iso3166FirstLevel,
        descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>
    ) : this(code3char.toAlias(), subdivision.toAlias(), linkedBy, descendants)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso3166SecondLevelOnlyParent::id,
            Iso3166SecondLevelOnlyParent::code3char,
            Iso3166SecondLevelOnlyParent::subdivision,
            Iso3166SecondLevelOnlyParent::linkedBy
        )
    }

    override fun toString(): String = kotlinToString(properties = properties)
}

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@DiscriminatorValue("C")
@Entity(name = "iso3166_2_C")
class Iso3166SecondLevelOnlyChild protected constructor(
    code3char: Code3CharAlias,
    subdivision: IsoToponymAlias,
    linkedBy: Iso3166FirstLevel,
    parent: Iso3166SecondLevel<*, HasChild>,
    internal: Boolean = true
) : Iso3166SecondLevel<HasParent, HasNoChild>(code3char, subdivision, linkedBy, parent, null), HasParent, HasNoChild {

    override var parent: Iso3166SecondLevel<*, HasChild>
        get() = super.parentProtected!!
        set(e) {
            super.parentProtected = e
        }

    constructor(
        code3char: Code3Char,
        subdivision: IsoToponym,
        linkedBy: Iso3166FirstLevel,
        parent: Iso3166SecondLevel<*, HasChild>
    ) : this(code3char.toAlias(), subdivision.toAlias(), linkedBy, parent)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso3166SecondLevelOnlyChild::id,
            Iso3166SecondLevelOnlyChild::code3char,
            Iso3166SecondLevelOnlyChild::subdivision,
            Iso3166SecondLevelOnlyChild::linkedBy,
            Iso3166SecondLevelOnlyChild::parent
        )
    }

    override fun toString(): String = kotlinToString(properties = properties)

}

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@DiscriminatorValue("L")
@Entity(name = "iso3166_2_L")
class Iso3166SecondLevelLonely protected constructor(
    code3char: Code3CharAlias,
    subdivision: IsoToponymAlias,
    linkedBy: Iso3166FirstLevel,
    internal: Boolean = true
) : Iso3166SecondLevel<HasNotParent, HasNoChild>(code3char, subdivision, linkedBy, null, null), HasNotParent,
    HasNoChild {
    constructor(
        code3char: Code3Char,
        subdivision: IsoToponym,
        linkedBy: Iso3166FirstLevel
    ) : this(code3char.toAlias(), subdivision.toAlias(), linkedBy)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso3166SecondLevelLonely::id,
            Iso3166SecondLevelLonely::code3char,
            Iso3166SecondLevelLonely::subdivision,
            Iso3166SecondLevelLonely::linkedBy
        )
    }

    override fun toString(): String = kotlinToString(properties = properties)

}

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id"
)
@JsonIgnoreProperties(value = ["coded"], allowGetters = true)
@DiscriminatorValue("S")
@Entity(name = "iso3166_2_S")
class Iso3166SecondLevelParentNChild protected constructor(
    code3char: Code3CharAlias,
    subdivision: IsoToponymAlias,
    linkedBy: Iso3166FirstLevel,
    parent: Iso3166SecondLevel<*, HasChild>,
    descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>,
    internal: Boolean = true
) : Iso3166SecondLevel<HasParent, HasChild>(code3char, subdivision, linkedBy, parent, descendants), HasParent,
    HasChild {

    override var descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>
        get() = super.descendantsProtected!!
        set(e) {
            super.descendantsProtected = e
        }

    override var parent: Iso3166SecondLevel<*, HasChild>
        get() = super.parentProtected!!
        set(e) {
            super.parentProtected = e
        }

    constructor(
        code3char: Code3Char,
        subdivision: IsoToponym,
        linkedBy: Iso3166FirstLevel,
        parent: Iso3166SecondLevel<*, HasChild>,
        descendants: MutableSet<Iso3166SecondLevel<HasParent, *>>
    ) : this(code3char.toAlias(), subdivision.toAlias(), linkedBy, parent, descendants)

    companion object {
        @Transient
        @JvmStatic
        private val properties = arrayOf(
            Iso3166SecondLevelParentNChild::id,
            Iso3166SecondLevelParentNChild::code3char,
            Iso3166SecondLevelParentNChild::subdivision,
            Iso3166SecondLevelParentNChild::linkedBy,
            Iso3166SecondLevelParentNChild::parent
        )
    }

    override fun toString(): String = kotlinToString(properties = properties)

}


fun Iso3166SecondLevel<*, *>.getParent(): Iso3166SecondLevel<*, HasChild>? =
    when (this) {
        is Iso3166SecondLevelLonely -> null
        is Iso3166SecondLevelOnlyChild -> this.parent
        is Iso3166SecondLevelOnlyParent -> null
        is Iso3166SecondLevelParentNChild -> this.parent
        else -> null
    }

fun Iso3166SecondLevel<*, *>.getDescendants(): Set<Iso3166SecondLevel<HasParent, *>>? =
    when (this) {
        is Iso3166SecondLevelLonely -> null
        is Iso3166SecondLevelOnlyChild -> null
        is Iso3166SecondLevelOnlyParent -> this.descendants
        is Iso3166SecondLevelParentNChild -> this.descendants
        else -> null
    }
