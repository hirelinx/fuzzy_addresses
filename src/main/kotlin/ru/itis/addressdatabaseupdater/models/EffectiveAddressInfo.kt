package ru.itis.addressdatabaseupdater.models

import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.IsoToponym

import ru.itis.addressdatabaseupdater.models.toAlias
import ru.itis.addressdatabaseupdater.models.toAlias
import ru.itis.addressdatabaseupdater.models.toAlias
import ru.itis.addressdatabaseupdater.models.toAlias
import jakarta.persistence.*
import org.geonames.Toponym
import ru.itis.addressdatabaseupdater.dto.Language
import ru.itis.addressdatabaseupdater.dto.FilePath

@Entity
class EffectiveAddressInfo (
    val filePath: FilePathAlias,
    val country: IsoToponymAlias,
    val language: LanguageAlias,

    @ElementCollection(targetClass = String::class, fetch = FetchType.EAGER)
    @CollectionTable(name = "subdivisions_parents", joinColumns = [JoinColumn(name = "country_id")])
    @Column(name = "subdivisions_parents", nullable = false)
    open var divisionsParents: List<IsoToponymAlias>?,

    @ElementCollection(targetClass = String::class, fetch = FetchType.EAGER)
    @CollectionTable(name = "subdivisions_selectors", joinColumns = [JoinColumn(name = "country_id")])
    @Column(name = "subdivisions_selectors", nullable = false)
    open var divisionsSelectors: Set<IsoToponymAlias>?,

    @ElementCollection(targetClass = String::class, fetch = FetchType.EAGER)
    @CollectionTable(name = "cities_selectors", joinColumns = [JoinColumn(name = "country_id")])
    @Column(name = "cities_selectors", nullable = false)
    open var citySelectors: Set<IsoToponymAlias>?,
    internal: Boolean = true
) {
    @Id
    @GeneratedValue
    open var id: Long? = null

    constructor(
        filePath: FilePath,
        country: IsoToponym,
        language: Language,
        divisionsParents: List<IsoToponym>?,
        citySelectors: Set<IsoToponym>,
        cityDiscriminator: Boolean = true
    ) : this(filePath.toAlias(), country.toAlias(), language.toAlias(),divisionsParents?.map { it.toAlias() }, null,citySelectors.map {  it.toAlias()}.toSet())


    constructor(
        filePath: FilePath,
        country: IsoToponym,
        language: Language,
        divisionsParents: List<IsoToponym>?,
        divisionsSelectors: Set<IsoToponym>,
    ) : this(filePath.toAlias(), country.toAlias(), language.toAlias(),divisionsParents?.map { it.toAlias() }, divisionsSelectors.map { it.toAlias() }.toSet(),
        null
    )

    constructor(
        filePath: FilePath,
        country: IsoToponym,
        language: Language,
    ) : this(filePath.toAlias(), country.toAlias(), language.toAlias(),null, null, null
    )

    override fun toString(): String {
        return "EffectiveAddressInfo(citySelectors=$citySelectors, filePath='$filePath', country='$country', language='$language', divisionsParents=$divisionsParents, divisionsSelectors=$divisionsSelectors, id=$id)"
    }

}