package ru.itis.addressdatabaseupdater.models


enum class LabelsEnum {
    house,
    category,
    near,
    house_number,
    road,
    unit,
    level,
    staircase,
    entrance,
    po_box,
    postcode,
    suburb,
    city_district,
    city,
    island,
    state_district,
    state,
    country_region,
    country,
    world_region;
}

val LABELS_ENUM_LENGTH: Int = LabelsEnum.entries.size



