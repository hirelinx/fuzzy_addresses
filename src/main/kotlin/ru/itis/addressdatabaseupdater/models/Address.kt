package ru.itis.addressdatabaseupdater.models

import com.mapzen.jpostal.ParsedComponent


class Address private constructor(private var values: Array<String?> = arrayOfNulls(LABELS_ENUM_LENGTH)) {
    constructor(
        components: Array<ParsedComponent>,
        essentialOrder: Boolean = false,
        labelsOrdinalsProvider: (String) -> Int?
    ) : this(
        if (essentialOrder) {
            Array<String?>(LABELS_ENUM_LENGTH) { i ->
                components.getOrNull(i)?.value
            }
        } else {
            val vals: Array<String?> = Array(LABELS_ENUM_LENGTH) { null }
            components.forEach { entry ->
                vals[labelsOrdinalsProvider(entry.label) ?: throw IllegalArgumentException()] = entry.value
            }
            vals
        }
    )

    constructor(
        labelsEnumHelper: EnumHelper<LabelsEnum>,
        components: Array<ParsedComponent>,
        essentialOrder: Boolean = false
    ) :
            this(components, essentialOrder, { ord -> labelsEnumHelper.getOrdinal(ord) })


    constructor(
        house: String? = null, category: String? = null, near: String? = null, house_number: String? = null,
        road: String? = null, unit: String? = null, level: String? = null, staircase: String? = null,
        entrance: String? = null, po_box: String? = null, postcode: String? = null, suburb: String? = null,
        city_district: String? = null, city: String? = null, island: String? = null,
        state_district: String? = null, state: String? = null, country_region: String? = null,
        country: String? = null, world_region: String? = null,
    )
            : this(
        arrayOf(
            house, category, near, house_number, road, unit, level, staircase, entrance, po_box,
            postcode, suburb, city_district, city, island, state_district, state, country_region,
            country, world_region,
        )
    )


    var house: String?
        get() = values[0]
        set(house) {
            values[0] = house
        }
    var category: String?
        get() = values[1]
        set(category) {
            values[1] = category
        }
    var near: String?
        get() = values[2]
        set(near) {
            values[2] = near
        }
    var house_number: String?
        get() = values[3]
        set(house_number) {
            values[3] = house_number
        }
    var road: String?
        get() = values[4]
        set(road) {
            values[4] = road
        }

    var unit: String?
        get() = values[5]
        set(unit) {
            values[5] = unit
        }

    var level: String?
        get() = values[6]
        set(level) {
            values[6] = level
        }

    var staircase: String?
        get() = values[7]
        set(staircase) {
            values[7] = staircase
        }

    var entrance: String?
        get() = values[8]
        set(entrance) {
            values[8] = entrance
        }

    var po_box: String?
        get() = values[9]
        set(po_box) {
            values[9] = po_box
        }

    var postcode: String?
        get() = values[10]
        set(postcode) {
            values[10] = postcode
        }

    var suburb: String?
        get() = values[11]
        set(suburb) {
            values[11] = suburb
        }

    var city_district: String?
        get() = values[12]
        set(city_district) {
            values[12] = city_district
        }

    var city: String?
        get() = values[13]
        set(city) {
            values[13] = city
        }

    var island: String?
        get() = values[14]
        set(island) {
            values[14] = island
        }

    var state_district: String?
        get() = values[15]
        set(state_district) {
            values[15] = state_district
        }

    var state: String?
        get() = values[16]
        set(state) {
            values[16] = state
        }

    var country_region: String?
        get() = values[17]
        set(country_region) {
            values[17] = country_region
        }

    var country: String?
        get() = values[18]
        set(country) {
            values[18] = country
        }

    var world_region: String?
        get() = values[19]
        set(world_region) {
            values[19] = world_region
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Address

        return values.contentEquals(other.values)
    }

    override fun hashCode(): Int {
        return values.contentHashCode()
    }

    override fun toString(): String {
        return "Address(house=$house, category=$category, near=$near, house_number=$house_number, road=$road, unit=$unit, level=$level, staircase=$staircase, entrance=$entrance, po_box=$po_box, postcode=$postcode, suburb=$suburb, city_district=$city_district, city=$city, island=$island, state_district=$state_district, state=$state, country_region=$country_region, country=$country, world_region=$world_region)"
    }
}

//class Example
//
//interface Laughable {
//    fun laugh()
//}
//
//fun Example.laugh() {
//    println(this.toString())
//}
//
//class FuckUrself(val example: Example): Laughable by object : Laughable{ override fun laugh() = example.laugh() }
//
//class Derived(val fuckUrself: FuckUrself) : Laughable {
//    override fun laugh() = fuckUrself.laugh()
//
//}
