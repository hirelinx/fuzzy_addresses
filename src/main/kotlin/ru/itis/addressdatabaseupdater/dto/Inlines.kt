package ru.itis.addressdatabaseupdater.dto

import jakarta.persistence.Column
import jakarta.persistence.Embeddable
import jakarta.validation.constraints.Size




interface Code {
    val value: String
}

interface Encoded {
    val name: String
}

@JvmInline
value class Code2Char(
    @Size(min = 2, max = 2)
    override val value: String
) : Code {
    constructor() : this("**")
}


@JvmInline
value class Code3Char(
    @Size(min=1, max = 3)
    override val value: String
) : Code {
    constructor() : this("***")
}

@JvmInline
value class IsoToponym(override val name: String) : Encoded

@JvmInline
value  class Language(override val name: String) : Encoded

@JvmInline
value class FilePath(val name: String)