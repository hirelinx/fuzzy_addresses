package ru.itis.addressdatabaseupdater.dto

import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size

class Country2LanguagesDTO (
    @get:NotNull @get:Size(min = 2, max = 2)
    val country: String,
    @get:NotNull
    val languages: MutableSet<@Size(min = 2, max = 2) String>
)