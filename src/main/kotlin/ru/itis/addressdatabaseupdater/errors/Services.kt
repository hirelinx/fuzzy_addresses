package ru.itis.addressdatabaseupdater.errors


import org.springframework.core.NestedRuntimeException
import org.springframework.dao.DataAccessException
import org.springframework.dao.OptimisticLockingFailureException
import ru.itis.addressdatabaseupdater.utils.annotations.CommonsUnsafe
import ru.itis.addressdatabaseupdater.utils.commons.*

@JvmInline
value class SpringDataJdbcHandler(override val exception: DataAccessException) :
    OccurredInLanguagesRepository, ExceptionCarrier<DataAccessException>,
    ScopeProvider3<DataAccessException, InternallyOccurred, ExceptionCarrier<DataAccessException>>, HibernateSupport {
    companion object {
        fun handle(e: NestedRuntimeException) = SpringDataJdbcHandler(e as DataAccessException)

        @CommonsUnsafe.UnsafeAs
        fun getScopeStatic() = SpringDataJdbcHandler(LightWeighter.getThrowable<DataAccessException>()).getScope()
    }
}

@JvmInline
value class IllegalArgumentHandler(override val exception: IllegalArgumentException) :
    OccurredInLanguagesRepository, ExceptionCarrier<IllegalArgumentException>,
    ScopeProvider3<IllegalArgumentException, InternallyOccurred, ExceptionCarrier<IllegalArgumentException>> {
    companion object {
        fun handle(e: IllegalArgumentException) = IllegalArgumentHandler(e)

        @CommonsUnsafe.UnsafeAs
        fun getScopeStatic() = IllegalArgumentHandler(LightWeighter.getThrowable<IllegalArgumentException>()).getScope()
    }
}

@JvmInline
value class OptimisticLockingFailureHandler(override val exception: OptimisticLockingFailureException) :
    OccurredInLanguagesRepository, ExceptionCarrier<OptimisticLockingFailureException> {
    companion object {
        fun handle(e: OptimisticLockingFailureException) = OptimisticLockingFailureHandler(e)

        @CommonsUnsafe.UnsafeAs
        fun getScopeStatic() =
            OptimisticLockingFailureHandler(LightWeighter.getThrowable<OptimisticLockingFailureException>()).getScope()
    }
}

fun OccurredInLanguagesRepository.getAnyException() = when (this) {
    is IllegalArgumentHandler -> exception
    is OptimisticLockingFailureHandler -> exception
    is SpringDataJdbcHandler -> exception
}

class LanguagesRepositoryException : RuntimeException, InternalException<OccurredInLanguagesRepository> {
    companion object {
        fun <T> with(block: LanguagesRepositoryException.() -> T) = with(internal, block)

        @OptIn(CommonsUnsafe.UnsafeAs::class)
        @JvmStatic
        private val internal =
            LanguagesRepositoryException(IllegalArgumentHandler(LightWeighter.getThrowable<IllegalArgumentException>()))
    }

    val carrier: OccurredInLanguagesRepository

//    @CommonsUnsafe.UnsafeAs
//    override fun <E: Exception, C> getHandler(kClassE: KClass<E>, kClassC: KClass<C>) : ((e: E) ->  C)? where C : InternallyOccurred, C : ExceptionCarrier<E> = when (kClassE) {
//        IllegalArgumentException::class -> {it -> kClassC.unsafeAs(IllegalArgumentHandler.handle(it as IllegalArgumentException)) }
//        OptimisticLockingFailureException::class -> {it -> kClassC.unsafeAs(OptimisticLockingFailureHandler.handle(it as OptimisticLockingFailureException))}
//        else -> null
//    }

    constructor(carrier: OccurredInLanguagesRepository, message: String) : super(
        message,
        carrier.getAnyException()
    ) {
        this.carrier = carrier
    }

    constructor(
        carrier: OccurredInLanguagesRepository,
    ) : super("exception in languages repository") {
        this.carrier = carrier
    }

    override fun new(carrier: OccurredInLanguagesRepository, message: String) =
        LanguagesRepositoryException(carrier, message)

    override fun new(carrier: OccurredInLanguagesRepository) = LanguagesRepositoryException(carrier)
}