package ru.itis.addressdatabaseupdater.errors

import ru.itis.addressdatabaseupdater.utils.commons.InternallyOccurred
import ru.itis.addressdatabaseupdater.utils.commons.ExceptionRethrower

sealed interface InternalException<T: InternallyOccurred>: ExceptionRethrower<T>
sealed interface OccurredInLanguagesRepository: InternallyOccurred
