package ru.itis.addressdatabaseupdater.views

object IsoViews {

    interface General

    interface FlatMapLanguage  : General
    interface FlatMapCountry  : General
    interface FlatMapSubdivisionNonRecursiveInternally : General
    interface FlatMapSubdivisionRecursiveInternally : FlatMapSubdivisionNonRecursiveInternally
    interface GeneralRecursive : FlatMapLanguage, FlatMapCountry, FlatMapSubdivisionRecursiveInternally, FlatMapSubdivisionNonRecursiveInternally
}

