package ru.itis.addressdatabaseupdater

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class AddressDatabaseUpdaterApplication


fun main(args: Array<String>) {
    runApplication<AddressDatabaseUpdaterApplication>(*args)
}



class ServletInitializer : SpringBootServletInitializer() {
    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(AddressDatabaseUpdaterApplication::class.java)
    }
}
