package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding
import java.nio.file.Path

@ConfigurationProperties(prefix = "open-cages-data")
data class OpenCagesInfo @ConstructorBinding constructor(
    val countryToLanguagePath: Path,
    val countriesPath: Path,
    val statesPath: Path,
    val countiesPath: Path
)