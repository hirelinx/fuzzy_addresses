package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding
import java.nio.file.Path

@ConfigurationProperties(prefix = "adapters")
class AdaptersInfo @ConstructorBinding constructor (
    val openAddressesPathAdapter: Path,
    val legacyIsoAdapter: Path,
    val country2LanguageAdapter: Path
)