package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties(prefix = "geonames")
data class GeonamesInfo @ConstructorBinding constructor(val account: GeonamesAccount) {
    data class GeonamesAccount @ConstructorBinding constructor(val username: String)
}

