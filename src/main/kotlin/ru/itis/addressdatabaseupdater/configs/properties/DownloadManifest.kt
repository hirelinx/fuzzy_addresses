package ru.itis.addressdatabaseupdater.configs.properties

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import ru.itis.addressdatabaseupdater.utils.extraction.ArchiveFormat
import ru.itis.addressdatabaseupdater.utils.extraction.ArchiveFormats

enum class Formats {
    zip;

    companion object {
        // Converts the specified format of type Formats to the corresponding ArchiveFormat.
        fun toArchiveFormat(format: Formats): ArchiveFormat = when (format) {
            zip -> ArchiveFormats.Zip
        }

        @JvmStatic
        fun kotlinValueOf(value: String): Formats? = when (value) {
            zip.name -> zip
            else -> null
        }
    }
}


data class DownloadManifest(
    val sources: List<Source>
) {
    class Source(
        val name: String,
        format: Formats,
        val filter: Filter?,
    ) {
        val format: ArchiveFormat

        init {
            this.format = setFormat(format.name)
        }
        // Converts the specified format to the corresponding ArchiveFormat.
        @JsonProperty("format")
        fun setFormat(format: String): ArchiveFormat =
            Formats.kotlinValueOf(format)?.let { Formats.toArchiveFormat(it) } ?: throw IllegalArgumentException("unsupported format: $format")

        override fun toString(): String {
            return "Source(name='$name', filter=$filter, format=$format)"
        }


        @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type"
        )
        @JsonSubTypes(
            JsonSubTypes.Type(value = RegexFilter::class, name = "regex")
        )
        abstract class Filter

        data class RegexFilter(val descriptor: RegexDescriptor) : Filter() {
            data class RegexDescriptor(@JsonProperty("apply-to") val applyTo: Applicables, val value: Regex) {
                enum class Applicables {
                    EACH_FILE,
                    EACH_FOLDER,
                    ROOT
                }
            }
        }
    }


}


