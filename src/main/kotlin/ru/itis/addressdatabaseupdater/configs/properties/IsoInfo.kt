package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import kotlin.io.path.Path

@ConfigurationProperties(prefix = "iso")
data class IsoInfo(
    val iso3166first: FolderNFilter,
    val iso639: FolderNFilter,
    val iso3166second: FolderNFilterNSelector,
) {
    class FolderNFilter(folder: String, filter: String) {
        val folder = Path(folder)
        val filter = Regex(filter)
    }
    class FolderNFilterNSelector(folder: String, filter: String, countryCodeSelector: String) {
        val folder = Path(folder)
        val filter = Regex(filter)
        val countryCodeSelector = Regex(countryCodeSelector)
    }
}