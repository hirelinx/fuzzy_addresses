package ru.itis.addressdatabaseupdater.configs.properties

import net.harawata.appdirs.AppDirs
import net.harawata.appdirs.AppDirsFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.system.ApplicationPid
import org.springframework.stereotype.Component
import java.nio.file.Path
import kotlin.io.path.Path

@Component
final class SystemInfo(@Autowired applicationInfo: ApplicationInfo) {
    val pid: String = ApplicationPid().toString()
    val tempDir: Path = Path(System.getProperty("java.io.tmpdir"))
    val userDataDir: Path
    val userDataDirRoaming: Path
    val userConfigDir: Path
    val userConfigDirRoaming: Path
    val userCacheDir: Path
    val userLogDir: Path
    val userDownloadsDir: Path
    val siteDataDir: Path
    val siteDataDirMultiPath: Path
    val siteConfigDir: Path
    val siteConfigDirMultiPath: Path
    val sharedDir: Path

    init {
        val appDirs: AppDirs = AppDirsFactory.getInstance()

        userDataDir = Path(
            appDirs.getUserDataDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        userDataDirRoaming = Path(
            appDirs.getUserDataDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group,
                true
            )
        )
        userConfigDir = Path(
            appDirs.getUserConfigDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        userConfigDirRoaming = Path(
            appDirs.getUserConfigDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group,
                true
            )
        )
        userCacheDir = Path(
            appDirs.getUserCacheDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        userLogDir = Path(
            appDirs.getUserLogDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        userDownloadsDir = Path(
            appDirs.getUserDownloadsDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        siteDataDir = Path(
            appDirs.getSiteDataDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        siteDataDirMultiPath = Path(
            appDirs.getSiteDataDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group,
                true
            )
        )
        siteConfigDir = Path(
            appDirs.getSiteConfigDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
        siteConfigDirMultiPath = Path(
            appDirs.getSiteConfigDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group,
                true
            )
        )
        sharedDir = Path(
            appDirs.getSharedDir(
                applicationInfo.name,
                applicationInfo.version,
                applicationInfo.group
            )
        )
    }



    override fun toString(): String {
        return "SystemInfo(pid='$pid', tempDir=$tempDir, userDataDir=$userDataDir, userDataDirRoaming=$userDataDirRoaming, userConfigDir=$userConfigDir, userConfigDirRoaming=$userConfigDirRoaming, userCacheDir=$userCacheDir, userLogDir=$userLogDir, userDownloadsDir=$userDownloadsDir, siteDataDir=$siteDataDir, siteDataDirMultiPath=$siteDataDirMultiPath, siteConfigDir=$siteConfigDir, siteConfigDirMultiPath=$siteConfigDirMultiPath, sharedDir=$sharedDir)"
    }
}
