package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties(prefix = "application-info")
data class ApplicationInfo @ConstructorBinding constructor(
    val name: String,
    val group: String,
    val version: String
)