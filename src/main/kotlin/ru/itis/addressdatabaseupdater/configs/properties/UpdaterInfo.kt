package ru.itis.addressdatabaseupdater.configs.properties

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import ru.itis.addressdatabaseupdater.configs.properties.UpdaterInfo.Variables.applicationInfo
import ru.itis.addressdatabaseupdater.configs.properties.UpdaterInfo.Variables.systemInfo
import ru.itis.addressdatabaseupdater.utils.escaping.replaceEscapeCharsAndVariables
import java.nio.file.Path
import kotlin.io.path.*
import kotlin.time.Duration as KotlinDuration
import java.time.Duration as JavaDuration
import kotlin.time.toKotlinDuration


@ConfigurationProperties(prefix = "updater")
data class UpdaterConfig(
    internal val extraction: ExtractionConfig,
    internal val dataRoot: String = "{app-home}/{app-name}",
    internal val downloadManifestPath: String = "{root}/download.manifest",
    internal val downloadsPath: String = "{root}/downloads",
    internal val downloadDatesPath: String = "{root}/download.dates",
    internal val skipStages: List<InternalStages> = emptyList()
) {
    data class ExtractionConfig(
        internal val temporaryDirectory: TemporaryDirectoryConfig,
        internal val timeout: JavaDuration = JavaDuration.ofSeconds(30)
    ) {
        data class TemporaryDirectoryConfig @Throws(IllegalArgumentException::class) constructor(
            internal val location: String = "{sys-temp}/{app-name}/{pid}",
            internal val deleteOnExit: Boolean = false,
//            val creationMask: String = "700"
        )
    }
}


@Component
class UpdaterInfo @Throws(IllegalArgumentException::class) constructor(
    updaterConfig: UpdaterConfig
) {
    final val dataRoot: Path
    final val downloadManifestPath: Path
    final val downloadsPath: Path
    final val downloadDatesPath: Path
    val skipStages: List<InternalStages> = updaterConfig.skipStages

    init {
        val convertPathsWithVars: (String) -> String =
            { replaceEscapeCharsAndVariables(varNameToString = this::varsToValue, string = it) }
        this.dataRoot = Path(convertPathsWithVars(updaterConfig.dataRoot))
        require(this.dataRoot.let { it.isReadable() && it.isWritable() && it.isExecutable() && it.isDirectory() }) { "invalid path: ${this.dataRoot} should be readable,writable,executable directory" }
        this.downloadManifestPath = Path(convertPathsWithVars(updaterConfig.downloadManifestPath))
        require(this.downloadManifestPath.let { it.isReadable() && it.isRegularFile() }) { "invalid path: ${this.downloadManifestPath} should be readable,writable regular file" }
        this.downloadsPath = Path(convertPathsWithVars(updaterConfig.downloadsPath))
        require(this.downloadsPath.let { it.isReadable() && it.isWritable() && it.isExecutable() && it.isDirectory() }) { "invalid path: ${this.downloadsPath} should be readable,writable directory" }
        this.downloadDatesPath = Path(convertPathsWithVars(updaterConfig.downloadDatesPath))
        require(this.downloadDatesPath.let { it.isReadable() && it.isWritable() && it.isExecutable() && it.isDirectory() }) { "invalid path: ${this.downloadDatesPath} should be readable,writable directory" }
    }

    val extraction = this.ExtractionInfo(updaterConfig.extraction)

    inner class ExtractionInfo @Throws(IllegalArgumentException::class) constructor(
        extractionConfig: UpdaterConfig.ExtractionConfig
    ) {
        val temporaryDirectory: TemporaryDirectoryInfo =
            this.TemporaryDirectoryInfo(extractionConfig.temporaryDirectory)
        val timeout: KotlinDuration = extractionConfig.timeout.toKotlinDuration()

        inner class TemporaryDirectoryInfo @Throws(IllegalArgumentException::class) constructor(
            temporaryDirectoryConfig: UpdaterConfig.ExtractionConfig.TemporaryDirectoryConfig
        ) {
            val location: Path
            val deleteOnExit: Boolean = temporaryDirectoryConfig.deleteOnExit

            init {
                val convertPathsWithVars: (String) -> String =
                    { replaceEscapeCharsAndVariables(varNameToString = this@UpdaterInfo::varsToValue, string = it) }
                this.location = Path(convertPathsWithVars(temporaryDirectoryConfig.location))
//                require(this.location.let { it.isReadable() && it.isWritable() && it.isExecutable() && it.isDirectory() }) { "invalid path: ${this.location} should be readable,writable,executable directory" }
            }

            override fun toString(): String {
                return "TemporaryDirectoryConfig(deleteOnExit=$deleteOnExit, location=$location)"
            }
        }

        override fun toString(): String {
            return "ExtractionConfig(temporaryDirectory=$temporaryDirectory, timeout=$timeout)"
        }
    }

    @Component
    companion object Variables {
        lateinit var systemInfo: SystemInfo
        lateinit var applicationInfo: ApplicationInfo

        @Autowired
        fun init(systemInfo: SystemInfo, applicationInfo: ApplicationInfo) {
            this.systemInfo = systemInfo
            this.applicationInfo = applicationInfo
        }

        override fun toString(): String {
            return "Variables(systemInfo=$systemInfo, applicationInfo=$applicationInfo)"
        }
    }

    override fun toString(): String {
        return "InternalInfo(dataRoot=$dataRoot, downloadManifestPath=$downloadManifestPath, downloadsPath=$downloadsPath, downloadDatesPath=$downloadDatesPath, skipStages=$skipStages, extraction=$extraction)"
    }
}

fun UpdaterInfo.varsToValue(variable: String): String? = when (variable) {
    "app-name", "APP-NAME", "application-name", "APPLICATION-NAME" -> applicationInfo.name
    "pid", "PID", "process-id", "PROCESS-ID" -> systemInfo.pid
    "sys-temp", "SYS-TEMP", "sys-tmp", "SYS-TMP", "tmpdir", "TMPDIR", "tempdir", "TEMPDIR",
    "temporary-directory", "TEMPORARY-DIRECTORY", "tmp", "TMP", "temp", "TEMP" -> systemInfo.tempDir.toString()

    "app-home", "APP-HOME", "application-home", "APPLICATION-HOME" -> systemInfo.userDataDir.toString()
    "data-root", "DATA-ROOT", "root", "ROOT" -> this.dataRoot.toString()
    else -> null
}

enum class InternalStages {
    CHECK_FOR_DOWNLOAD,
    DOWNLOAD,
    UNPACK,
    UPDATE
}



