package ru.itis.addressdatabaseupdater.configs.beans

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import net.lingala.zip4j.model.UnzipParameters
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import ru.itis.addressdatabaseupdater.configs.properties.OpenCagesInfo
import ru.itis.addressdatabaseupdater.models.EnumHelper
import ru.itis.addressdatabaseupdater.models.LabelsEnum
import ru.itis.addressdatabaseupdater.models.buildEnumHelperImpl
import ru.itis.addressdatabaseupdater.utils.extraction.*
import kotlin.io.path.useLines
import ru.itis.addressdatabaseupdater.configs.properties.AdaptersInfo


@Configuration
class InternalBeans {
//    @Bean
//    fun language2CountryMap(@Autowired config: OpenCagesInfo): Map<String, Set<String>> {
//        val transform = Regex("""(?<!\[)[^\[\] ",\n]+(?!\[)""")
//        val mapper = ObjectMapper(YAMLFactory()).registerModules(KotlinModule.Builder().build())
//        return mapper.readValue<Map<String, String>>(config.countryToLanguagePath.toFile().inputStream()).map{
//            out -> (transform.find(out.key)?.value ?: throw IllegalArgumentException("")) to transform.findAll(out.value).map{ it.value.uppercase() }.toSet() }.toMap();
//    }

    @Bean
    fun archivedFileStreamer() = ArchivedFileStreamer.instance(descriptor = Zip4JProvider.getDescriptor(),
        fileStreamState = FileStreamState(
            iterateState = IterateState(
                filesFilter = {fd -> fd.holden.fileName.matches(Regex(""".*meta.*"""))},
                archiveInformation = Zip4JInformation(UnzipParameters())
            ),
            streamsNewURIProvider = { _, fd -> fd.holden.fileName }
        )
    )


    @Bean
    @Primary
    fun objectMapper(builder: Jackson2ObjectMapperBuilder): ObjectMapper {
        return builder.defaultViewInclusion(true).build()
    }

    @Bean
    @Qualifier("country2LanguageAdapterDescriptor")
    fun country2LanguageAdapterDescriptor(info: AdaptersInfo) = info.country2LanguageAdapter.useLines {
        it.map { it.split(" ").let{ Pair(it[0].uppercase(), it[1].uppercase()) } }.toMap()
    }

    @Bean
    @Qualifier("legacyIsoAdapterDescriptor")
    fun legacyIsoAdapterDescriptor(info: AdaptersInfo) = info.legacyIsoAdapter.useLines {
        it.map { it.split(" ").let{ Pair(it[0].uppercase(), it[1].uppercase()) } }.toMap()
    }

    @Bean
    @Qualifier("openAddressesPathAdapterDescriptor")
    fun openAddressesPathAdapterDescriptor(info: AdaptersInfo) = info.openAddressesPathAdapter.useLines {
        it.map { it.split(" ").let{ Pair(it[0].uppercase(), it[1].uppercase()) } }.toMap()
    }


    @Bean
    @Qualifier("YamlMapper")
    fun yamlMapper() = ObjectMapper(YAMLFactory()).registerModules(KotlinModule.Builder().build())


//    @Qualifier("hibernate")
//    @Bean
//    fun hibernateValidator() {
//
//    }

    @ExperimentalStdlibApi
    @Qualifier("labelsEnumHelper")
    @Bean fun labelsEnumHelper() : EnumHelper<LabelsEnum> = buildEnumHelperImpl<LabelsEnum>()
}