package ru.itis.addressdatabaseupdater.configs.beans

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.itis.addressdatabaseupdater.configs.properties.DownloadManifest
import ru.itis.addressdatabaseupdater.configs.properties.UpdaterInfo

@Configuration
class ConfigurationBeans {
    @Bean
    fun downloadManifest(updaterInfo: UpdaterInfo, objectMapper: ObjectMapper): DownloadManifest =
        objectMapper.readValue(updaterInfo.downloadManifestPath.toFile(), DownloadManifest::class.java)
}


