package ru.itis.addressdatabaseupdater.parsers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.mapzen.jpostal.ParsedComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import ru.itis.addressdatabaseupdater.models.Address
import ru.itis.addressdatabaseupdater.models.EnumHelper
import ru.itis.addressdatabaseupdater.models.GeoJson
import ru.itis.addressdatabaseupdater.models.LabelsEnum
import java.io.BufferedReader
import java.io.InputStream



@OptIn(ExperimentalCoroutinesApi::class)
fun CoroutineScope.convertFilesToAddresses(
    converter: OpenAddressesConverter,
    input: ReceiveChannel<Pair<String, InputStream>>
): ReceiveChannel<Triple<String, Address?, Exception?>> = produce {
    with(converter) {
        for ((path, inputStream) in input) {
            BufferedReader(inputStream.reader()).useLines { lines ->
                for (line in lines) {
                    try {
                        send(Triple(path, convertLineToAddress(path, line)  ,null))
                    } catch (e: Exception) {
                        send(Triple(path, null,  e))
                    }
                }
            }
        }
    }
}

fun send(element: Triple<String, Pair<Address, Nothing?>, Exception>) {
    //TODO
}

@Component
class OpenAddressesConverter(
    @Qualifier("labelsEnumHelper") val helper: EnumHelper<LabelsEnum>,
    val mapper: ObjectMapper
) {
    @Throws(JsonProcessingException::class, JsonMappingException::class)
    fun convertLineToAddress(path: String, line: String): Address =
        convertGeoJsonToAddress(path, convertLineToGeoJson(line))

    @Throws(JsonProcessingException::class, JsonMappingException::class)
    fun convertLineToGeoJson(line: String): GeoJson = mapper.readValue(line, GeoJson::class.java)


    fun convertGeoJsonToAddress(path: String, geoJson: GeoJson): Address = Address(
            labelsEnumHelper = helper,
            components = arrayOf(ParsedComponent(geoJson.toString(), LabelsEnum.house.toString()))
    )
}
