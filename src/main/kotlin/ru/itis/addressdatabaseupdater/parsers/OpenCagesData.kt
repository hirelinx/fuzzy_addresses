package ru.itis.addressdatabaseupdater.parsers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import ru.itis.addressdatabaseupdater.dto.Code
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.Country2LanguagesDTO
import ru.itis.addressdatabaseupdater.dto.IsoToponym
import ru.itis.addressdatabaseupdater.utils.annotations.CommonsUnsafe
import ru.itis.addressdatabaseupdater.utils.commons.unsafeAs
import java.io.InputStream
import java.nio.file.Path
import kotlin.io.path.inputStream

interface OpenCagesDataParser {
    fun parse(inputStream: InputStream): List<Country2LanguagesDTO>
    fun parseStatesNCounties(
        statesPath: InputStream,
        countiesPath: InputStream,
        adapter: Country2LanguagesAdapter,
        mapper: ObjectMapper,
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym
    ): Pair<Map<String, MutableList<Pair<Code3Char, IsoToponym>>>, Map<String, MutableList<Pair<Code3Char, IsoToponym>>>>
}

@Component
class OpenCagesDataParserImpl : OpenCagesDataParser {
    override fun parse(inputStream: InputStream): List<Country2LanguagesDTO> {
        val transform = Regex("""(?<!\[)[^\[\] ",\n]+(?!\[)""")
        val mapper = ObjectMapper(YAMLFactory()).registerModules(KotlinModule.Builder().build())
        return mapper.readValue<Map<String, String>>(inputStream).map { out ->
            (transform.find(out.key)?.value ?: throw IllegalArgumentException("")) to transform.findAll(out.value)
                .map { it.value.uppercase() }.toMutableSet()
        }.map { Country2LanguagesDTO(it.first, it.second) }
    }


    override fun parseStatesNCounties(
        statesPath: InputStream,
        countiesPath: InputStream,
        adapter: Country2LanguagesAdapter,
        mapper: ObjectMapper,
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym
    ): Pair<Map<String, MutableList<Pair<Code3Char, IsoToponym>>>, Map<String, MutableList<Pair<Code3Char, IsoToponym>>>> {

        val counties = map<Code3Char>(countiesPath, stringToCode3, stringToToponym, mapper, adapter)
        val states = map<Code3Char>(statesPath, stringToCode3, stringToToponym, mapper, adapter)

        return states to counties
    }

    fun <C : Code> parse(
        map: Pair<String, Map<String, String>>,
        stringToCode: (String) -> C,
        stringToToponym: (String) -> IsoToponym
    ): Pair<C, IsoToponym> = stringToCode(map.first) to stringToToponym(map.second["default"]!!)

    @OptIn(CommonsUnsafe.UnsafeAs::class)
    fun <C : Code> map(
        inputStream: InputStream,
        stringToCode: (String) -> C,
        stringToToponym: (String) -> IsoToponym,
        mapper: ObjectMapper,
        adapter: Country2LanguagesAdapter,
    ) = mapper.readValue<Map<String, Any>>(inputStream).map { it ->
        it.key to it.value
            .let { id: Any ->
                when (id) {
                    is Map<*, *> -> (id.unsafeAs<Map<String, Any>>()).map { a ->
                        a.key to a.value.let { b ->
                            when (b) {
                                is Map<*, *> -> (b.unsafeAs<Map<String, String>>()).map { entry ->
                                    (when (entry.key) {
                                        "default" -> entry.key; "alt" -> entry.key; else -> entry.key.substring(4)
                                            .let {
                                                adapter.adapt(it)
                                            }
                                    }) to entry.value
                                }.toMap()

                                is String -> mapOf("default" to b)
                                else -> throw IllegalArgumentException("not readable")
                            }
                        }.toMap()
                    }
                    else -> throw IllegalArgumentException("not readable")
                }.map { a -> parse(a, stringToCode, stringToToponym) }
            }.toMutableList()
    }.toMap()
}




