package ru.itis.addressdatabaseupdater.parsers

import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.IsoToponym
import ru.itis.addressdatabaseupdater.dto.Language
import ru.itis.addressdatabaseupdater.models.*

interface Source<Any>

typealias StringToCode2Char = (String) -> Code2Char
typealias StringToCode3Char = (String) -> Code3Char
typealias StringToLanguage = (String) -> Language
typealias StringToIsoToponym = (String) -> IsoToponym

//enum class AddressSourcesList: Source<Address> {
//    OpenAddresses
//}
//
//enum class ISOSourcesList: Source<Iso> {
//    Wiki, OpenCageData
//}