package ru.itis.addressdatabaseupdater.parsers

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import ru.itis.addressdatabaseupdater.dto.Country2LanguagesDTO


interface LegacyIsoAdapter {
    fun adapt(iso: String): String
}

@Component
class LegacyIsoAdapterImpl(@Qualifier("legacyIsoAdapterDescriptor") val adapterDescriptor: Map<String, String>): LegacyIsoAdapter {
    override fun adapt(iso: String): String = if (adapterDescriptor.containsKey(iso)) adapterDescriptor[iso]!! else iso
}

interface OpenAddressesPathAdapter{
    fun adapt(path: String) : String
}

@Component
class OpenAddressesPathAdapterImpl(@Qualifier("openAddressesPathAdapterDescriptor") val adapterDescriptor: Map<String, String>): OpenAddressesPathAdapter {
    override fun adapt(path: String): String  = if (adapterDescriptor.containsKey(path)) adapterDescriptor[path]!! else path
}

interface Country2LanguagesAdapter {
    fun adapt(adaptee: Country2LanguagesDTO): Country2LanguagesDTO
    fun adapt(adaptee: String): String
}

@Component
class Country2LanguagesAdapterImpl(@Qualifier("country2LanguageAdapterDescriptor") val adapterDescriptor: Map<String, String>) :
    Country2LanguagesAdapter {
    override fun adapt(adaptee: Country2LanguagesDTO): Country2LanguagesDTO =
        adaptee.also {
            it.languages.map {
                if (adapterDescriptor.containsKey(it)) {
                    adapterDescriptor[it]
                } else {
                    it
                }
            }
        }

    override fun adapt(adaptee: String): String = if (adapterDescriptor.containsKey(adaptee)) adapterDescriptor[adaptee]!! else adaptee

}