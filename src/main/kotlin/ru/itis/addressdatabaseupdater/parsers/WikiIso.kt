package ru.itis.addressdatabaseupdater.parsers

import org.jetbrains.kotlinx.dataframe.DataFrame
import org.jetbrains.kotlinx.dataframe.io.*
import org.jetbrains.kotlinx.dataframe.api.*
import org.jetbrains.kotlinx.dataframe.columns.ColumnAccessor
import ru.itis.addressdatabaseupdater.models.*
import org.springframework.stereotype.Component
import org.apache.poi.util.ReplacingInputStream
import ru.itis.addressdatabaseupdater.dto.Code2Char
import ru.itis.addressdatabaseupdater.dto.Code3Char
import ru.itis.addressdatabaseupdater.dto.IsoToponym
import ru.itis.addressdatabaseupdater.models.Iso3166FirstLevel
import ru.itis.addressdatabaseupdater.utils.annotations.CommonsUnsafe
import ru.itis.addressdatabaseupdater.utils.commons.unsafeAs
import java.io.InputStream
import java.nio.file.Path
import java.time.format.DateTimeFormatter
import java.util.*


typealias ManyToOneRelation = List<Pair<Code3Char, Code3Char>>
typealias ToponymsOfCode = Pair<Code3Char, List<IsoToponym>>

interface WikiIsoParser {
    //    fun <E : Iso> parseCsv(file: InputStream, iso: KClass<E>): Iterable<E>


    fun parseCsvToIso639Level1(
        file: InputStream,
        stringToCode2: StringToCode2Char,
        stringToCode3: StringToCode3Char,
        stringToLanguage: StringToLanguage
    ): Iterable<Iso639FirstLevel>

    fun parseCsvToIso3166Level1(
        file: InputStream,
        stringToCode2: StringToCode2Char,
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym
    ): Iterable<Iso3166FirstLevel>

    fun parseCsvToIso3166Level2(
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym,
        paths: List<Path>,
        linkedBy: Code2Char,
        codeColumnName: Regex = Regex("Code")
    ): List<Pair<List<ManyToOneRelation>?, List<ToponymsOfCode>>>
}

//inline fun <reified E : Iso> WikiIsoParser.parseCsv(file: InputStream) = this.parseCsv<E>(file, E::class)


@Component
class WikiIsoParserImpl(
    val parserOptions: ParserOptions = ParserOptions(
        nullStrings = setOf(),
        dateTimePattern = "",
        dateTimeFormatter = DateTimeFormatter.ofPattern(""),
        locale = Locale.UK
    )
) : WikiIsoParser {
//    override fun <E : Iso> parseCsv(file: InputStream, iso: KClass<E>): Iterable<E> =
//        when (iso) {
//            Iso639FirstLevel::class -> parseCsvToIso3166Level1(file)
//            Iso3166FirstLevel::class -> parseCsvToIso3166Level2(file)
//            Iso3166SecondLevel::class -> parseCsvToIso639Level1(file)
//            else -> throw IllegalArgumentException("bad class: ${iso.qualifiedName}")
//        }


    @OptIn(CommonsUnsafe.UnsafeAs::class)
    fun wikiIsoParentToChildren(
        path: Path,
        stringToCode: (String) -> Code3Char,
        stringToToponym: StringToIsoToponym,
        countryCode: Code2Char,
        codeColumnRegex: Regex,
        parentNuller: (Any?) -> Any? = { if (it != "—" && it != "-") it else null },
        ignoreTablesContainsColumn: Regex = Regex("""absorbed by""", RegexOption.IGNORE_CASE),
    ): Pair<List<ManyToOneRelation>?, List<ToponymsOfCode>>? {
        val data = DataFrame.readCSV(path.toFile(), parserOptions = parserOptions)
            .let { it.convert(*it.columnNames().toTypedArray()).to<String?>() }
        val selectUrls = Regex(""".*(_url_\d*)""")
        val parrentMatcher = Regex("""( |^)([Ii]n)|([Pp]arent)( |${'$'})""")
        val subdivisionsMatcher = Regex("""(.*subdivision name.*)|(^name$)""", RegexOption.IGNORE_CASE)
        val subdivisionsName = "Subdivisions"
        if (data.columnNames().filter { !selectUrls.matches(it) && ignoreTablesContainsColumn.containsMatchIn(it) }.size > 0) {
            return null
        }

        val codeColumnName =
            data.columnNames().filter { !selectUrls.matches(it) && codeColumnRegex.containsMatchIn(it) }
                .let { if (it.size > 0) it[0] else throw IllegalStateException("no code column") }

        val parentColumnName =
            data.columnNames().filter { !selectUrls.matches(it) && parrentMatcher.containsMatchIn(it) }
                .let { if (it.size > 0) it[0] else null }


        val relation = parentColumnName?.let {

            val flatten =
                data.select(codeColumnName, parentColumnName).update(parentColumnName).with { parentNuller(it) }
                    .groupBy(parentColumnName).values(codeColumnName)

            flatten.map { row ->
                (row[0]?.let { stringToCode(it as String) } to (row[1]!!.unsafeAs<List<String>>()).map { stringToCode(it) })
                    .let { if (it.first == null ) null else it }
            }.filterNotNull().map { (f, s) -> s.map { it to f as Code3Char } }
        }

        val subdivisions =
            (data.columnNames().filter { !selectUrls.matches(it) && subdivisionsMatcher.containsMatchIn(it) }
                .toTypedArray())

        assert (subdivisions.size !=0 ){"no subdivisions column in $path"}

        val names = data.select(codeColumnName, *subdivisions).let {
            it.merge(*subdivisions)
                .into(subdivisionsName)
                .rows()
                .map {
                    ((it.get(codeColumnName) as? String)?.let { it1 -> stringToCode(it1) } to (it.get(subdivisionsName) as? List<String?>)?.filterNotNull()
                        ?.map(stringToToponym))
                }.filter { if (it.first == null || it.second == null) false else true }
                .map { it as Pair<Code3Char, List<IsoToponym>> }
        }
        return relation to names

    }

    override fun parseCsvToIso3166Level2(
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym,
        paths: List<Path>,
        linkedBy: Code2Char,
        codeColumnName: Regex
    ): List<Pair<List<ManyToOneRelation>?, List<ToponymsOfCode>>> {
        return paths.map { path ->
            wikiIsoParentToChildren(
                path,
                stringToCode3,
                stringToToponym,
                linkedBy,
                codeColumnName
            )
        }.filterNotNull()
    }


    override fun parseCsvToIso639Level1(
        file: InputStream,
        stringToCode2: StringToCode2Char,
        stringToCode3: StringToCode3Char,
        stringToLanguage: StringToLanguage
    ): Iterable<Iso639FirstLevel> {
        val input = ReplacingInputStream(file, "\u00A0", " ")
        val data = DataFrame.readCSV(input, parserOptions = ParserOptions(nullStrings = setOf()))
        val nameSel by column<String>("ISO language names")
        val set2sel by column<String>("Set 1")
        val set3sel by column<String>("Set 2 /T")

        val nameRegex = Regex("[^  ]*")
        val set1Regex = Regex("""..""")
        val set2Regex = Regex("""...""")

        var temp = data.select { nameSel and set2sel and set3sel }

        fun <E> findByRegex(df: DataFrame<E>, selector: ColumnAccessor<String>, regex: Regex) =
            df.update(selector).with { s -> regex.find(s)?.value ?: throw IllegalArgumentException("bad source") }
        temp = findByRegex(temp, nameSel, nameRegex)
        temp = findByRegex(temp, set2sel, set1Regex)
        temp = findByRegex(temp, set3sel, set2Regex)

        temp = temp.update(set2sel, set3sel).with { it.uppercase() }

        return temp.merge { nameSel and set2sel and set3sel }
            .by {
                Iso639FirstLevel(
                    stringToCode2(it[1]),
                    stringToCode3(it[2]),
                    stringToLanguage(it[0]),
                    mutableSetOf()
                )
            }.intoList()
    }

    override fun parseCsvToIso3166Level1(
        file: InputStream,
        stringToCode2: StringToCode2Char,
        stringToCode3: StringToCode3Char,
        stringToToponym: StringToIsoToponym
    ): Iterable<Iso3166FirstLevel> {
        val input = ReplacingInputStream(file, "\u00A0", " ")
        val data2 = DataFrame.readCSV(input, parserOptions = ParserOptions(nullStrings = setOf()))
        val nameSel by column<String>("English short name (using title case )")
        val set2sel by column<String>("Alpha-2 code")
        val set3sel by column<String>("Alpha-3 code")
        val nameRegex = Regex("""(?<!\[)[^\[\] "\n]+( [^\[\] "\n]+)*(?!\[)""")
        val set2Regex = Regex("""..""")
        val set3Regex = Regex("""...""")
        var temp = data2.select { nameSel and set2sel and set3sel }

        fun <E> findByRegex(df: DataFrame<E>, selector: ColumnAccessor<String>, regex: Regex) =
            df.update(selector).with { s -> regex.findAll(s).joinToString(separator = " ") { it.value } }
        temp = findByRegex(temp, nameSel, nameRegex)
        temp = findByRegex(temp, set2sel, set2Regex)
        temp = findByRegex(temp, set3sel, set3Regex)

        temp = temp.update(set2sel, set3sel).with { it.uppercase() }

        return temp.merge { nameSel and set2sel and set3sel }.by {
            Iso3166FirstLevel(
                stringToCode2(it[1]),
                stringToCode3(it[2]),
                stringToToponym(it[0]),
                mutableSetOf(),
                mutableSetOf()
            )
        }.intoList()
    }
}