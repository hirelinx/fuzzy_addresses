@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("io.freefair.lombok") version "8.1.0"
    war
    id ("application")
    id("org.jetbrains.kotlinx.dataframe") version "0.13.1"
    id("org.springframework.boot") version "3.2.4"
    id("io.spring.dependency-management") version "1.1.4"
//    kotlin("plugin.serialization") version "1.9.22"
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.spring") version "1.9.22"

    id("idea")

    kotlin("plugin.jpa") version "1.9.22"


    id("org.jetbrains.kotlin.plugin.allopen") version "1.9.22"
}



idea {
    module {
        isDownloadJavadoc = true
        isDownloadSources = true
    }
}

group = "ru.itis"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_20
}

repositories {
    flatDir {
        dirs = setOf(file("locallib"))
    }
    mavenCentral()
    jcenter()
}

allOpen {
    annotation("jakarta.persistence.Entity")
}

dependencies {
    // https://mvnrepository.com/artifact/net.harawata/appdirs
    implementation("net.harawata:appdirs:1.2.2")
    implementation(files("locallib/jpostal.jar"))
    providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
    implementation("org.springframework.boot:spring-boot-starter-pulsar")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    // https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-data-jpa
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:3.2.5")
    // https://mvnrepository.com/artifact/jakarta.persistence/jakarta.persistence-api
    implementation("jakarta.persistence:jakarta.persistence-api:3.1.0")
    testImplementation("org.springframework.security:spring-security-test")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    // https://mvnrepository.com/artifact/net.lingala.zip4j/zip4j
    implementation("net.lingala.zip4j:zip4j:2.11.5")

    // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/dataframe
    implementation("org.jetbrains.kotlinx:dataframe:0.13.1")
    implementation("org.jetbrains.kotlinx:dataframe-jdbc:0.13.1")

    // https://mvnrepository.com/artifact/com.neovisionaries/nv-i18n
    implementation("com.neovisionaries:nv-i18n:1.29")
    implementation("com.wolt.osm:parallelpbf:0.3.1")
    implementation("com.google.cloud:google-cloud-translate:2.42.0")
    // https://mvnrepository.com/artifact/org.geonames/geonames-ws-client
    implementation("org.geonames:geonames-ws-client:1.1.17")
    // https://mvnrepository.com/artifact/com.h2database/h2
    implementation("com.h2database:h2:2.2.224")
//// https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-noarg
//    implementation("org.jetbrains.kotlin:kotlin-noarg:1.9.24")
    // https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-aop
    implementation("org.springframework.boot:spring-boot-starter-aop:3.2.5")
    // https://mvnrepository.com/artifact/org.springframework/spring-instrument
    implementation("org.springframework:spring-instrument:6.1.6")

    // https://mvnrepository.com/artifact/org.aspectj/aspectjrt
    runtimeOnly("org.aspectj:aspectjrt:1.9.22")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    runtimeOnly("com.h2database:h2")
    providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.springframework.security:spring-security-test")
    implementation("com.github.pemistahl:lingua:1.2.2")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.9.22")

// https://mvnrepository.com/artifact/net.bytebuddy/byte-buddy
    implementation("net.bytebuddy:byte-buddy:1.14.15")

// https://mvnrepository.com/artifact/org.aspectj/aspectjweaver
    runtimeOnly("org.aspectj:aspectjweaver:1.9.22")


    // https://mvnrepository.com/artifact/org.apache.commons/commons-lang3
    implementation("org.apache.commons:commons-lang3:3.14.0")


// https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-kotlin
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.17.1")

    // https://mvnrepository.com/artifact/au.com.console/kassava
    implementation("au.com.console:kassava:2.1.0")

    // https://mvnrepository.com/artifact/org.apache.poi/poi
    implementation("org.apache.poi:poi:5.2.5")
// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation
    implementation("org.springframework.boot:spring-boot-starter-validation:3.2.5")

    // https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator
    implementation("org.hibernate.validator:hibernate-validator:8.0.1.Final")
// https://mvnrepository.com/artifact/org.postgresql/postgresql
    implementation("org.postgresql:postgresql:42.7.3")

    implementation("me.xdrop:fuzzywuzzy:1.4.0")
}

//allOpen {     annotation("javax.persistence.Entity") }


application {
    applicationDefaultJvmArgs = listOf(
        "--add-opens java.base/java.lang=ALL-UNNAMED")
}


tasks.withType<ProcessResources> {
    expand(project.properties) {
        escapeBackslash = true
    }
}

tasks.withType<KotlinCompile> {

    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "20"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<JavaExec> {
    systemProperty("java.library.path", "locallib")
}


//noArg {
//    annotation("ru.itis.addressdatabaseupdater.utils.annotations.NoArgsConstructor")
//    invokeInitializers = true
//}
